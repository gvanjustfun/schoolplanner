package com.useful_things.android.schoolplanner.ui.teachers.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.databinding.ComposeSubjectBinding;
import com.useful_things.android.schoolplanner.ui.subjects.entity.Subject;
import com.useful_things.android.schoolplanner.ui.teachers.ComposeSubjectsPresenter;
import com.useful_things.android.schoolplanner.ui.teachers.view_holder.ComposeSubjectsViewHolder;

import java.util.List;

public class ComposeSubjectsAdapter extends RecyclerView.Adapter<ComposeSubjectsViewHolder> {

    private ComposeSubjectsPresenter presenter;
    private List<Subject> subjects;

    public ComposeSubjectsAdapter(ComposeSubjectsPresenter presenter) {
        this.presenter = presenter;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ComposeSubjectsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        ComposeSubjectBinding binding = DataBindingUtil.inflate(inflater, R.layout.compose_subject, viewGroup, false);
        return new ComposeSubjectsViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ComposeSubjectsViewHolder holder, int i) {
        Subject subject = subjects.get(i);
        holder.setSubject(subject, presenter);
    }

    @Override
    public int getItemCount() {
        int count = 0;
        if(subjects != null)
            count = subjects.size();
        return count;
    }
}
