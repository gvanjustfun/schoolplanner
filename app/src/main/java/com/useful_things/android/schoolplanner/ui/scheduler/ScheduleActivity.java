package com.useful_things.android.schoolplanner.ui.scheduler;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.PopupMenu;
import android.view.View;

import com.useful_things.android.schoolplanner.Const;
import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.Utils;
import com.useful_things.android.schoolplanner.data.entity.SubjectEntity;
import com.useful_things.android.schoolplanner.databinding.BaseFrameBinding;
import com.useful_things.android.schoolplanner.databinding.BasePagerBinding;
import com.useful_things.android.schoolplanner.ui.base.App;
import com.useful_things.android.schoolplanner.ui.base.BaseActivity;
import com.useful_things.android.schoolplanner.ui.scheduler.adapter.SchedulePagerAdapter;

import java.util.Calendar;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;


public class ScheduleActivity extends BaseActivity {

    private BasePagerBinding binding;
    private CompositeDisposable disposables;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.base_pager);
        disposables = new CompositeDisposable();

        Calendar calendar = Calendar.getInstance();
        int weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);

        binding.tabs.setupWithViewPager(binding.pager);
        binding.pager.setAdapter(new SchedulePagerAdapter(getSupportFragmentManager(), weekOfYear, this));
        binding.pager.setCurrentItem(8);

        binding.toolbar.toolbarTitle.setText(R.string.schedule);

        initDb();
    }

    @Override
    protected void onStart() {
        super.onStart();
        binding.navigationBottom.setSelectedItemId(R.id.navigation_schedule);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Const.OPEN_HOMEWORK_CODE: {
                if(resultCode == RESULT_OK) {
                    String fragmentTag = String.format(Const.ANDROID_SWITHCER, R.id.pager, binding.pager.getCurrentItem());
                    Fragment fragment = getSupportFragmentManager().findFragmentByTag(fragmentTag);
                    if(fragment != null && fragment instanceof ScheduleView) {
                        ((ScheduleView)fragment).loadSchedule();
                    }
                }
                break;
            }
            case Const.CHOOSE_SUBJECT_CODE:{
                if(resultCode == RESULT_OK && data != null) {
                    int dayOfWeek = data.getIntExtra(Const.DAY_OF_WEEK, 0);
                    int numberInDay = data.getIntExtra(Const.NUMBER_IN_DAY, 0);
                    String key = data.getStringExtra(Const.KEY);
                    for(int i = -1;i < 2;i++) {
                        int position = binding.pager.getCurrentItem() + i;
                        if(position >= 0 && position < binding.pager.getAdapter().getCount()) {
                            String fragmentTag = String.format(Const.ANDROID_SWITHCER, R.id.pager, position);
                            Fragment fragment = getSupportFragmentManager().findFragmentByTag(fragmentTag);
                            if(fragment != null && fragment instanceof ScheduleView) {
                                ((ScheduleView)fragment).addSubject(dayOfWeek, numberInDay, key);
                            }
                        }
                    }

                }
                break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(disposables != null) disposables.dispose();
    }

    private void initDb(){
        Disposable disposable = App.getApp().getDb().subjectDao().getAll()
                .subscribe(new Consumer<List<SubjectEntity>>() {
                    @Override
                    public void accept(List<SubjectEntity> subjectEntities) throws Exception {
                        if(subjectEntities.size() == 0)
                            addSubjectsToDb();
                    }
                });
        disposables.add(disposable);
    }

    private void addSubjectsToDb(){
        String[] subjects = getResources().getStringArray(R.array.subjects_ua);
        SubjectEntity[] entities = new SubjectEntity[subjects.length];
        for(int i = 0;i < subjects.length;i++) {
            entities[i] = new SubjectEntity();
            entities[i].setSubject_key(subjects[i]);
        }
        Disposable disposable = Completable.fromAction(() -> App.getApp().getDb().subjectDao().insert(entities))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Action() {
                @Override
                public void run() throws Exception {

                }
            });
        disposables.add(disposable);
    }

}
