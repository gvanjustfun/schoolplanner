package com.useful_things.android.schoolplanner.ui.subjects;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.useful_things.android.schoolplanner.Const;
import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.databinding.BaseFrameBinding;
import com.useful_things.android.schoolplanner.ui.base.BaseActivity;

public class ChooseSubjectsActivity extends BaseActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        BaseFrameBinding binding = DataBindingUtil.setContentView(this, R.layout.base_frame);
        binding.toolbar.toolbarBack.setVisibility(View.VISIBLE);
        binding.toolbar.toolbarTitle.setText(R.string.choose_subject);
        binding.toolbar.toolbarAction1.setVisibility(View.VISIBLE);
        binding.toolbar.toolbarAction1.setImageResource(R.drawable.ic_plus);
        binding.toolbar.toolbarAction1.setOnClickListener(v -> openAddSubject());

        Bundle arguments = new Bundle();
        arguments.putInt(Const.DAY_OF_WEEK, getIntent().getIntExtra(Const.DAY_OF_WEEK, 0));
        arguments.putInt(Const.NUMBER_IN_DAY, getIntent().getIntExtra(Const.NUMBER_IN_DAY, 0));

        Fragment fragment = new ChooseSubjectsViewImpl();
        fragment.setArguments(arguments);
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_frame, fragment).commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Const.ADD_SUBJECT_CODE:{
                if(resultCode == RESULT_OK) {
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_frame);
                    if(fragment != null && fragment instanceof ChooseSubjectsView) {
                        ((ChooseSubjectsView) fragment).loadSubjects();
                    }
                }
                break;
            }
        }
    }

    private void openAddSubject(){
        Intent intent = new Intent(this, AddSubjectActivity.class);
        startActivityForResult(intent, Const.ADD_SUBJECT_CODE);
    }

}
