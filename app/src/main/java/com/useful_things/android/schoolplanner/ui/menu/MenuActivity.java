package com.useful_things.android.schoolplanner.ui.menu;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.databinding.BaseFrameBinding;
import com.useful_things.android.schoolplanner.ui.base.BaseActivity;

public class MenuActivity extends BaseActivity {

    private BaseFrameBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.base_frame);
        binding.toolbar.toolbarTitle.setText(R.string.menu);

        Fragment fragment = new MenuViewImpl();
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_frame, fragment).commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        binding.navigationBottom.setSelectedItemId(R.id.navigation_menu);
    }
}
