package com.useful_things.android.schoolplanner.data.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "schedules")
public class ScheduleEntity {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int schedule_id;
    @ColumnInfo(name = "semester_id")
    private int semester_id;
    @ColumnInfo(name = "day_of_week")
    private int day_of_week;
    @ColumnInfo(name = "number_in_day")
    private int number_in_day;
    @ColumnInfo(name = "subject_key")
    private String subject_key;

    @NonNull
    public int getSchedule_id() {
        return schedule_id;
    }

    public void setSchedule_id(@NonNull int schedule_id) {
        this.schedule_id = schedule_id;
    }

    public int getSemester_id() {
        return semester_id;
    }

    public void setSemester_id(int semester_id) {
        this.semester_id = semester_id;
    }

    public int getDay_of_week() {
        return day_of_week;
    }

    public void setDay_of_week(int day_of_week) {
        this.day_of_week = day_of_week;
    }

    public int getNumber_in_day() {
        return number_in_day;
    }

    public void setNumber_in_day(int number_in_day) {
        this.number_in_day = number_in_day;
    }

    public String getSubject_key() {
        return subject_key;
    }

    public void setSubject_key(String subject_key) {
        this.subject_key = subject_key;
    }
}
