package com.useful_things.android.schoolplanner.ui.homework;

import com.useful_things.android.schoolplanner.ui.base.App;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class HomeworkPresenterImpl extends HomeworkPresenter{

    public HomeworkPresenterImpl(String dateStr, int numberInDay) {
        super(dateStr, numberInDay);
    }

    @Override
    public void saveHomework() {
        String homework = view.getHomework();
        boolean done = view.isDone();

        Disposable disposable = Completable
                .fromAction(() -> App.getApp().getDb().homeworkDao()
                        .insertOrUpdate(dateStr, numberInDay, homework, done))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    if (view == null) return;

                    view.finishActivity();
                }, throwable -> throwable.printStackTrace());
        compositeDisposable.add(disposable);
    }
}
