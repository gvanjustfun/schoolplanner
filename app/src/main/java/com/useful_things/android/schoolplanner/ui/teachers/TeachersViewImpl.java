package com.useful_things.android.schoolplanner.ui.teachers;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.useful_things.android.schoolplanner.Const;
import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.databinding.RecyclerBinding;
import com.useful_things.android.schoolplanner.ui.teachers.adapter.TeachersAdapter;
import com.useful_things.android.schoolplanner.ui.teachers.entity.Teacher;

import java.util.List;

public class TeachersViewImpl extends TeachersView {

    private RecyclerBinding binding;
    private TeachersAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        presenter = new TeachersPresenterImpl();
        adapter = new TeachersAdapter(presenter);

        binding = DataBindingUtil.inflate(inflater, R.layout.recycler, container, false);
        binding.recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recycler.setAdapter(adapter);
        binding.swipeRefresh.setColorSchemeResources(R.color.colorPrimary);
        binding.swipeRefresh.setOnRefreshListener(() -> presenter.loadTeachers());

        binding.progress.progress.setVisibility(View.VISIBLE);
        presenter.loadTeachers();

        return binding.getRoot();
    }

    @Override
    public void hideProgress() {
        binding.progress.progress.setVisibility(View.GONE);
        binding.swipeRefresh.setRefreshing(false);
    }

    @Override
    public void setTeachers(List<Teacher> teachers) {
        adapter.setTeachers(teachers);
    }

    @Override
    public void openTeacher(String hash) {
        Intent intent = new Intent(getActivity(), AddTeacherActivity.class);
        intent.putExtra(Const.HASH, hash);
        getActivity().startActivityForResult(intent, Const.CREATE_TEACHER_CODE);
    }

    @Override
    public void loadTeachers() {
        binding.progress.progress.setVisibility(View.VISIBLE);
        presenter.loadTeachers();
    }
}
