package com.useful_things.android.schoolplanner.ui.subjects;

import com.useful_things.android.schoolplanner.ui.base.BaseFragmentMVP;

public abstract class AddSubjectView extends BaseFragmentMVP<AddSubjectPresenter> {

    public abstract void closeWithResult();

}
