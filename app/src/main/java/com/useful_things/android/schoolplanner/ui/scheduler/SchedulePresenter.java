package com.useful_things.android.schoolplanner.ui.scheduler;

import com.useful_things.android.schoolplanner.ui.base.BaseFragmentPresenter;
import com.useful_things.android.schoolplanner.ui.scheduler.entity.Schedule;

public abstract class SchedulePresenter extends BaseFragmentPresenter<ScheduleView> {

    public static final int DEFAULT_SCHEDULE_AMOUNT = 8;
    public static int WEEK_SIZE = 6;

    public abstract void loadSchedule();
    public abstract void openSubjectsList(int dayOfWeek, int numberInDay);
    public abstract void addSubject(int dayOfWeek, int numberIdDay, String key);
    public abstract void openHomework(Schedule schedule);

}
