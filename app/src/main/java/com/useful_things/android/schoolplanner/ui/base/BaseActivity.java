package com.useful_things.android.schoolplanner.ui.base;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.PopupMenu;

import com.useful_things.android.schoolplanner.Const;
import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.ui.menu.MenuActivity;
import com.useful_things.android.schoolplanner.ui.scheduler.ScheduleActivity;
import com.useful_things.android.schoolplanner.ui.subjects.SubjectsActivity;
import com.useful_things.android.schoolplanner.ui.teachers.TeachersActivity;

public class BaseActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs = getSharedPreferences(Const.SETTINGS_PREFERENCES, MODE_PRIVATE);
        int theme = prefs.getInt(Const.APP_THEME, Const.THEME_LIGHT);
        switch (theme) {
            case Const.THEME_DARK:{
                setTheme(R.style.DarkTheme);
                break;
            }
            default:{
                setTheme(R.style.LightTheme);
                break;
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        BottomNavigationView navigationView = findViewById(R.id.navigation_bottom);
        navigationView.setOnNavigationItemSelectedListener(this);
        findViewById(R.id.toolbar_back).setOnClickListener(v -> closeActivity());
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.navigation_schedule:{
                if(!(this instanceof ScheduleActivity)) {
                    Intent intent = new Intent(this, ScheduleActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                }
                return true;
            }
            case R.id.navigation_subjects:{
                if(!(this instanceof SubjectsActivity)) {
                    Intent intent = new Intent(this, SubjectsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                }
                return true;
            }
            case R.id.navigation_teacher:{
                if(!(this instanceof TeachersActivity)) {
                    Intent intent = new Intent(this, TeachersActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                }
                return true;
            }
            case R.id.navigation_menu:{
                if(!(this instanceof MenuActivity)) {
                    Intent intent = new Intent(this, MenuActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                }
            }
        }
        return false;
    }

    private void closeActivity(){
        View focus = getCurrentFocus();
        if(focus != null) {
            InputMethodManager manager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            manager.hideSoftInputFromWindow(focus.getWindowToken(), 0);
        }
        finish();
    }

}
