package com.useful_things.android.schoolplanner.ui.subjects;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;

import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.databinding.BasePagerBinding;
import com.useful_things.android.schoolplanner.ui.base.BaseActivity;
import com.useful_things.android.schoolplanner.ui.subjects.adapter.SubjectsPagerAdapter;

public class SubjectsActivity extends BaseActivity {

    private BasePagerBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.base_pager);
        binding.toolbar.toolbarTitle.setText(R.string.subjects);

        binding.tabs.setTabMode(TabLayout.MODE_FIXED);
        binding.tabs.setupWithViewPager(binding.pager);
        binding.pager.setAdapter(new SubjectsPagerAdapter(getSupportFragmentManager(), this));
    }

    @Override
    protected void onStart() {
        super.onStart();
        binding.navigationBottom.setSelectedItemId(R.id.navigation_subjects);
    }
}
