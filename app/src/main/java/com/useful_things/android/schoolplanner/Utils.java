package com.useful_things.android.schoolplanner;

import android.util.Log;

import java.util.Calendar;

public class Utils {

    public static String formatDay(int day) {
        return day < 10 ? String.format("0%s", day) : String.valueOf(day);
    }

    public static String formatMonth(int month) {
        month += 1;
        return month < 10 ? String.format("0%s", month) : String.valueOf(month);
    }

    public static int getDayOfWeekName(int dayOfWeek) {
        switch (dayOfWeek) {
            case 0: return R.string.monday;
            case 1: return R.string.tuesday;
            case 2: return R.string.wednesday;
            case 3: return R.string.thursday;
            case 4: return R.string.friday;
            case 5: return R.string.saturday;
        }
        return R.string.str_empty;
    }

    public static int getCalendarToDayOfWeek(int calendar) {
        switch (calendar) {
            case Calendar.MONDAY: return 0;
            case Calendar.TUESDAY: return 1;
            case Calendar.WEDNESDAY: return 2;
            case Calendar.THURSDAY: return 3;
            case Calendar.FRIDAY: return 4;
            case Calendar.SATURDAY: return 5;
            case Calendar.SUNDAY: return 6;
        }
        return 0;
    }

    public static int getSubjectIdByKey(String key) {
        switch (key) {
            case Const.CHEMISTRY: return R.string.chemistry;
            case Const.PHYSICS: return R.string.physics;
            case Const.WORLD_LITERATURE: return R.string.world_literature;
            case Const.SPORTS: return R.string.sports;
            case Const.HISTORY: return R.string.history;
            case Const.GEOGRAPHY: return R.string.geography;
            case Const.GEOMETRY: return R.string.geometry;
            case Const.ALGEBRA: return R.string.algebra;
            case Const.SPANISH: return R.string.spanish;
            case Const.FRENCH: return R.string.french;
            case Const.ENGLISH: return R.string.music;
            case Const.MUSIC: return R.string.music;
            case Const.ART: return R.string.art;
            case Const.MATHEMATICS: return R.string.mathematics;
            case Const.BIOLOGY: return R.string.biology;
            case Const.UKRAINIAN_LANGUAGE: return R.string.ukrainian_language;
            case Const.UKRAINIAN_LITERATURE: return R.string.ukrainian_literature;
        }
        return R.string.subject;
    }

    public static void log(String str, Object... objects) {
        if(BuildConfig.DEBUG)
            Log.d(Const.TAG, String.format(str, objects));
    }

    public static void log(String str) {
        if(BuildConfig.DEBUG)
            Log.d(Const.TAG, str);
    }

}
