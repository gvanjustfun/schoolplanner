package com.useful_things.android.schoolplanner.ui.scheduler.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.useful_things.android.schoolplanner.Const;
import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.Utils;
import com.useful_things.android.schoolplanner.databinding.ScheduleEntryBinding;
import com.useful_things.android.schoolplanner.databinding.ScheduleHeaderBinding;
import com.useful_things.android.schoolplanner.ui.scheduler.SchedulePresenter;
import com.useful_things.android.schoolplanner.ui.scheduler.entity.Schedule;
import com.useful_things.android.schoolplanner.ui.scheduler.view_holder.ScheduleHeaderViewHolder;
import com.useful_things.android.schoolplanner.ui.scheduler.view_holder.ScheduleViewHolder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class ScheduleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private final List<Schedule[]> schedulesList = new ArrayList<>();
    private SchedulePresenter presenter;

    public ScheduleAdapter(SchedulePresenter presenter) {
        this.presenter = presenter;
    }

    public void setSchedules(List<Schedule[]> schedulesList) {
        if(schedulesList != null) {
            this.schedulesList.clear();
            this.schedulesList.addAll(schedulesList);
            notifyDataSetChanged();
        }
    }

    public int getScheduleSize(){
        return schedulesList.size();
    }

    @Override
    public int getItemViewType(int position) {
        int itemsCount = 0;
        for(Schedule[] schedules : schedulesList) {
            if(itemsCount == position) return Const.VIEW_TYPE_HEADER;
            itemsCount++;
            for(int i = 0;i < schedules.length;i++) {
                if(itemsCount == position) return Const.VIEW_TYPE_ENTRY;
                itemsCount++;
            }
        }
        return -1;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        switch (i) {
            case Const.VIEW_TYPE_HEADER:{
                ScheduleHeaderBinding binding = DataBindingUtil.inflate(inflater, R.layout.schedule_header, viewGroup, false);
                return new ScheduleHeaderViewHolder(binding);
            }
            case Const.VIEW_TYPE_ENTRY:{
                ScheduleEntryBinding binding = DataBindingUtil.inflate(inflater, R.layout.schedule_entry, viewGroup, false);
                return new ScheduleViewHolder(binding, presenter);
            }
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder baseHolder, int position) {
        switch (baseHolder.getItemViewType()) {
            case Const.VIEW_TYPE_ENTRY:{
                Schedule schedule = null;
                int itemsCount = 0;
                rootLoop:
                for(Schedule[] schedules : schedulesList) {
                    itemsCount++;
                    for(int i = 0;i < schedules.length;i++) {
                        if(itemsCount == position) {
                            schedule = schedules[i];
                            break rootLoop;
                        }
                        itemsCount++;
                    }
                }
                if(schedule != null) {
                    ScheduleViewHolder holder = (ScheduleViewHolder) baseHolder;
                    holder.setSchedule(schedule);
                }
                break;
            }
            case Const.VIEW_TYPE_HEADER:{
                String date = null;
                int itemsCount = 0;
                for(Schedule[] schedules : schedulesList) {
                    if(itemsCount == position) {
                        date = schedules[0].getDate();
                        break;
                    }
                    itemsCount++;
                    itemsCount += schedules.length;
                }
                if(date != null) {
                    ScheduleHeaderViewHolder holder = (ScheduleHeaderViewHolder) baseHolder;
                    holder.setDate(date);
                }
                break;
            }
        }
    }

    @Override
    public int getItemCount() {
        int count = 0;
        for(Schedule[] schedules : schedulesList) {
            count += schedules.length + 1;
        }
        return count;
    }
}
