package com.useful_things.android.schoolplanner.ui.subjects;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.useful_things.android.schoolplanner.Const;
import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.databinding.RecyclerBinding;
import com.useful_things.android.schoolplanner.ui.subjects.adapter.ChooseSubjectsAdapter;
import com.useful_things.android.schoolplanner.ui.subjects.entity.Subject;

import java.util.Collections;
import java.util.List;

public class ChooseSubjectsViewImpl extends ChooseSubjectsView {

    private RecyclerBinding binding;
    private ChooseSubjectsAdapter adapter;
    private int dayOfWeek = 0;
    private int numberIdDay = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        presenter = new ChooseSubjectsPresenterImpl();
        adapter = new ChooseSubjectsAdapter(presenter);

        dayOfWeek = getArguments().getInt(Const.DAY_OF_WEEK, 0);
        numberIdDay = getArguments().getInt(Const.NUMBER_IN_DAY, 0);

        binding = DataBindingUtil.inflate(inflater, R.layout.recycler, container, false);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        binding.recycler.setLayoutManager(layoutManager);
        binding.recycler.addItemDecoration(new DividerItemDecoration(getContext(), layoutManager.getOrientation()));
        binding.recycler.setAdapter(adapter);
        binding.swipeRefresh.setColorSchemeResources(R.color.colorPrimary);
        binding.swipeRefresh.setOnRefreshListener(() -> presenter.loadData());

        binding.progress.progress.setVisibility(View.VISIBLE);
        presenter.loadData();

        return binding.getRoot();
    }

    @Override
    public void hideProgress() {
        binding.progress.progress.setVisibility(View.GONE);
        binding.swipeRefresh.setRefreshing(false);
    }

    @Override
    public void setSubjects(List<Subject> subjects) {
        adapter.setSubjects(subjects);
    }

    @Override
    public void setResult(String key, String name) {
        Intent intent = new Intent();
        intent.putExtra(Const.KEY, key);
        intent.putExtra(Const.NAME, name);
        intent.putExtra(Const.DAY_OF_WEEK, dayOfWeek);
        intent.putExtra(Const.NUMBER_IN_DAY, numberIdDay);
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
    }

    @Override
    public void loadSubjects() {
        binding.progress.progress.setVisibility(View.VISIBLE);
        presenter.loadData();
    }
}
