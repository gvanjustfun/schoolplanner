package com.useful_things.android.schoolplanner.ui.base;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.useful_things.android.schoolplanner.data.AppDatabase;

public class App extends Application {

    private static App app;

    private AppDatabase db;
    private final String DB_NAME = "school_planner.db";
    private int semId = 1;

    @Override
    public void onCreate() {
        super.onCreate();

        app = this;

        db = Room.databaseBuilder(this, AppDatabase.class, DB_NAME).build();
    }

    public static App getApp() {
        return app;
    }

    public AppDatabase getDb() {
        return db;
    }

    public int getSemId() {
        return semId;
    }
}
