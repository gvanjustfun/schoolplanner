package com.useful_things.android.schoolplanner.ui.scheduler;

import com.useful_things.android.schoolplanner.ui.base.BaseFragmentMVP;
import com.useful_things.android.schoolplanner.ui.scheduler.entity.Schedule;

import java.util.List;

public abstract class ScheduleView extends BaseFragmentMVP<SchedulePresenter> {

    public abstract void setSchedule(List<Schedule[]> schedules);
    public abstract void openSubjectsList(int dayOfWeek, int numberInDay);
    public abstract void openHomework(Schedule schedule);
    public abstract void hideProgress();
    public abstract void loadSchedule();
    public abstract void addSubject(int dayOfWeek, int numberIdDay, String key);

}
