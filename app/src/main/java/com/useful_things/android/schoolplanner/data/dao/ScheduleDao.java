package com.useful_things.android.schoolplanner.data.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import com.useful_things.android.schoolplanner.data.entity.ScheduleEntity;
import com.useful_things.android.schoolplanner.ui.scheduler.entity.Schedule;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public abstract class ScheduleDao {

    @Insert
    public abstract void insert(ScheduleEntity ... scheduleEntities);

    @Query("SELECT * FROM schedules WHERE semester_id = :semesterId")
    public abstract Flowable<List<ScheduleEntity>> getSchedule(int semesterId);

    @Query("SELECT * FROM schedules WHERE semester_id = :semesterId AND day_of_week = :dayOfWeek")
    public abstract Flowable<List<ScheduleEntity>> getSchedule(int semesterId, int dayOfWeek);

    @Query("SELECT * FROM schedules WHERE semester_id = :semesterId AND day_of_week = :dayOfWeek AND number_in_day = :numberInDay")
    public abstract  List<ScheduleEntity> getSchedule(int semesterId, int dayOfWeek, int numberInDay);

    @Query("UPDATE schedules SET subject_key = :subjectKey WHERE semester_id = :semesterId AND day_of_week = :dayOfWeek AND number_in_day = :numberInDay")
    public abstract void update(int semesterId, int dayOfWeek, int numberInDay, String subjectKey);

    @Transaction
    public void insertOrUpdate(int semId, int dayOfWeek, int number, String subjectKey) {
        if(getSchedule(semId, dayOfWeek, number).size() > 0) {
            update(semId, dayOfWeek, number, subjectKey);
        } else {
            ScheduleEntity entity = new ScheduleEntity();
            entity.setSemester_id(semId);
            entity.setDay_of_week(dayOfWeek);
            entity.setNumber_in_day(number);
            entity.setSubject_key(subjectKey);
            insert(entity);
        }
    }

}
