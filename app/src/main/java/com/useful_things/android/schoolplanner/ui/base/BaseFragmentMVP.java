package com.useful_things.android.schoolplanner.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;

public abstract class BaseFragmentMVP<T extends BaseFragmentPresenter> extends BaseFragment {

    protected T presenter;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter.onAttach(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.onDetach();
    }

    public boolean isClosing(){
        return getActivity() == null || getActivity().isFinishing();
    }

}
