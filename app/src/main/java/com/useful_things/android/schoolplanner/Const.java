package com.useful_things.android.schoolplanner;

public interface Const {

    int VIEW_TYPE_HEADER = 0;
    int VIEW_TYPE_ENTRY = 1;

    int CREATE_TEACHER_CODE = 0;
    int CHOOSE_SUBJECT_CODE = 1;
    int OPEN_HOMEWORK_CODE = 2;
    int ADD_SUBJECT_CODE = 3;

    int THEME_LIGHT = 0;
    int THEME_DARK = 1;

    String APP_THEME = "APP_THEME";
    String SETTINGS_PREFERENCES = "settings_preferences";
    String NAME = "name";
    String HASH = "hash";
    String MODE = "mode";
    String DAY_OF_WEEK = "day_of_week";
    String WEEK_OF_YEAR = "week_of_year";
    String CUSTOM = "custom";
    String ANDROID_SWITHCER = "android:switcher:%s:%s";
    String HOMEWORK_TEXT = "homework_text";
    String NUMBER_IN_DAY = "number_in_day";
    String DATE = "date";
    String TIME_IN_MILLIS = "time_in_millis";
    String KEY = "key";
    String ID = "id";
    String TAG = "school";
    String DD_MMM_YYYY = "dd-MMM-yyyy";
    String EEEE_DD_MMMM = "EEEE, dd MMMM";
    String DD_MMM = "dd.MMM";
    String DAY = "day";

//    SUBJECTS
    String CHEMISTRY = "chemistry";
    String PHYSICS = "physics";
    String WORLD_LITERATURE = "world_literature";
    String SPORTS = "sports";
    String HISTORY = "history";
    String GEOGRAPHY = "geography";
    String GEOMETRY = "geometry";
    String ALGEBRA = "algebra";
    String SPANISH = "spanish";
    String FRENCH = "french";
    String ENGLISH = "english";
    String MUSIC = "music";
    String ART = "art";
    String MATHEMATICS = "mathematics";
    String BIOLOGY = "biology";
    String UKRAINIAN_LITERATURE = "ukrainian_literature";
    String UKRAINIAN_LANGUAGE = "ukrainian_language";
}
