package com.useful_things.android.schoolplanner.map;

import com.useful_things.android.schoolplanner.Utils;
import com.useful_things.android.schoolplanner.data.entity.SubjectEntity;
import com.useful_things.android.schoolplanner.ui.base.App;
import com.useful_things.android.schoolplanner.ui.subjects.entity.Subject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.functions.Function;

public class SubjectMapper implements Function<List<SubjectEntity>, List<Subject>> {

    @Override
    public List<Subject> apply(List<SubjectEntity> subjectEntities) throws Exception {
        List<Subject> subjects = new ArrayList<>();
        for(SubjectEntity subjectEntity : subjectEntities) {
            Subject subject = new Subject();
            if(subjectEntity.getSubject_name() == null) {
                subject.setSubject_name(App.getApp().getString(Utils.getSubjectIdByKey(subjectEntity.getSubject_key())));
            } else {
                subject.setSubject_name(subjectEntity.getSubject_name());
            }
            subject.setSubject_key(subjectEntity.getSubject_key());
            subjects.add(subject);
        }

        Collections.sort(subjects, (o1, o2) -> o1.getSubject_name().compareTo(o2.getSubject_name()));

        return subjects;
    }
}
