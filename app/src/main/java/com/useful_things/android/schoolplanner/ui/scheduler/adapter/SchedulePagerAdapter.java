package com.useful_things.android.schoolplanner.ui.scheduler.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.useful_things.android.schoolplanner.Const;
import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.ui.scheduler.ScheduleViewImpl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class SchedulePagerAdapter extends FragmentPagerAdapter {

    private final List<Integer> weekIds = new ArrayList<>();
    private final List<String> weekNames = new ArrayList<>();

    public SchedulePagerAdapter(FragmentManager fm, int currentWeek, Context context) {
        super(fm);
        weekIds.clear();
        weekNames.clear();

        for(int i = 8;i >= 2;i--) {
            int week = currentWeek - i;
            weekIds.add(week);
            weekNames.add(getWeekRange(week));
        }

        weekIds.add(currentWeek - 1);
        weekNames.add(context.getString(R.string.previous_week));

        weekIds.add(currentWeek);
        weekNames.add(context.getString(R.string.current_week));

        weekIds.add(currentWeek + 1);
        weekNames.add(context.getString(R.string.next_week));
    }

    private String getWeekRange(int week) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(Const.DD_MMM);
        Calendar first = Calendar.getInstance();
        Calendar last = Calendar.getInstance();

        first.set(Calendar.WEEK_OF_YEAR, week);
        first.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        last.set(Calendar.WEEK_OF_YEAR, week);
        last.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);

        return String.format("%s - %s",
                dateFormat.format(first.getTime()),
                dateFormat.format(last.getTime()));
    }

    @Override
    public Fragment getItem(int i) {
        Bundle arguments = new Bundle();
        arguments.putInt(Const.WEEK_OF_YEAR, weekIds.get(i));
        Fragment fragment = new ScheduleViewImpl();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if(position < weekNames.size())
            return weekNames.get(position);
        return "";
    }

    @Override
    public int getCount() {
        return weekIds.size();
    }
}
