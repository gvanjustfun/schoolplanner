package com.useful_things.android.schoolplanner.ui.scheduler;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.useful_things.android.schoolplanner.Const;
import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.Utils;
import com.useful_things.android.schoolplanner.databinding.RecyclerBinding;
import com.useful_things.android.schoolplanner.ui.homework.HomeworkActivity;
import com.useful_things.android.schoolplanner.ui.scheduler.adapter.ScheduleAdapter;
import com.useful_things.android.schoolplanner.ui.scheduler.entity.Schedule;
import com.useful_things.android.schoolplanner.ui.subjects.ChooseSubjectsActivity;

import java.util.Calendar;
import java.util.List;


public class ScheduleViewImpl extends ScheduleView {

    private RecyclerBinding binding;
    private ScheduleAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        int weekOfYear = getArguments().getInt(Const.WEEK_OF_YEAR, 0);

        presenter = new SchedulePresenterImpl(weekOfYear);
        adapter = new ScheduleAdapter(presenter);

        binding = DataBindingUtil.inflate(inflater, R.layout.recycler, container, false);
        binding.recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recycler.setAdapter(adapter);
        binding.swipeRefresh.setColorSchemeResources(R.color.colorPrimary);
        binding.swipeRefresh.setOnRefreshListener(() -> presenter.loadSchedule());

        loadSchedule();

        return binding.getRoot();
    }

    @Override
    public void setSchedule(List<Schedule[]> schedules) {
        boolean needScroll = adapter.getScheduleSize() == 0;

        adapter.setSchedules(schedules);

        if(needScroll) {
            Calendar calendar = Calendar.getInstance();
            int dayOfWeek = Utils.getCalendarToDayOfWeek(calendar.get(Calendar.DAY_OF_WEEK));
            int scrollPosition = dayOfWeek * (SchedulePresenter.DEFAULT_SCHEDULE_AMOUNT + 1);
            LinearLayoutManager manager = (LinearLayoutManager) binding.recycler.getLayoutManager();
            manager.scrollToPositionWithOffset(scrollPosition, 0);
        }
    }

    @Override
    public void openSubjectsList(int dayOfWeek, int numberInDay) {
        Intent intent = new Intent(getActivity(), ChooseSubjectsActivity.class);
        intent.putExtra(Const.DAY_OF_WEEK, dayOfWeek);
        intent.putExtra(Const.NUMBER_IN_DAY, numberInDay);
        getActivity().startActivityForResult(intent, Const.CHOOSE_SUBJECT_CODE);
    }

    @Override
    public void openHomework(Schedule schedule) {
        Intent intent = new Intent(getActivity(), HomeworkActivity.class);
        intent.putExtra(Const.DATE, schedule.getDate());
        intent.putExtra(Const.NUMBER_IN_DAY, schedule.getNumberInDay());
        intent.putExtra(Const.HOMEWORK_TEXT, schedule.getHomework());
        getActivity().startActivityForResult(intent, Const.OPEN_HOMEWORK_CODE);
    }

    @Override
    public void hideProgress() {
        binding.progress.progress.setVisibility(View.GONE);
        binding.swipeRefresh.setRefreshing(false);
    }

    @Override
    public void loadSchedule() {
        binding.progress.progress.setVisibility(View.VISIBLE);
        presenter.loadSchedule();
    }

    @Override
    public void addSubject(int dayOfWeek, int numberIdDay, String key) {
        presenter.addSubject(dayOfWeek, numberIdDay, key);
    }


}
