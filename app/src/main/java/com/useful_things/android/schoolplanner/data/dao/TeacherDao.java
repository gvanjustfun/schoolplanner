package com.useful_things.android.schoolplanner.data.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.useful_things.android.schoolplanner.data.entity.TeacherEntity;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public interface TeacherDao {

    @Query("SELECT * FROM teachers")
    Flowable<List<TeacherEntity>> getAll();

    @Query("SELECT * FROM teachers WHERE hash = :hash")
    Flowable<List<TeacherEntity>> getTeacher(String hash);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(TeacherEntity... teacherEntities);

    @Query("UPDATE teachers SET first_name = :first_name, last_name = :last_name, middle_name = :middle_name, phone = :phone, auditory = :auditory, birthday = :birthday WHERE hash = :hash")
    void update(String hash, String first_name, String last_name, String middle_name,String phone,String auditory, String birthday);

    @Delete
    void delete(TeacherEntity teacherEntity);

}
