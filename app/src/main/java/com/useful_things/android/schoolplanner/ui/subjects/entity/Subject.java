package com.useful_things.android.schoolplanner.ui.subjects.entity;

import java.util.ArrayList;
import java.util.List;

public class Subject {

    private String subject_key;
    private String subject_name;
    private List<Integer> daysOfWeek;
    private List<String> teachers;

    public String getSubject_key() {
        return subject_key;
    }

    public void setSubject_key(String subject_key) {
        this.subject_key = subject_key;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }

    public void addDayOfWeek(Integer day) {
        if(daysOfWeek == null)
            daysOfWeek = new ArrayList<>();

        if(!daysOfWeek.contains(day))
            daysOfWeek.add(day);
    }

    public List<Integer> getDaysOfWeek() {
        if(daysOfWeek == null)
            daysOfWeek = new ArrayList<>();
        return daysOfWeek;
    }

    public List<String> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<String> teachers) {
        this.teachers = teachers;
    }
}
