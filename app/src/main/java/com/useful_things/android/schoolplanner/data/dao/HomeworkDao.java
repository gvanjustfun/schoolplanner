package com.useful_things.android.schoolplanner.data.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.useful_things.android.schoolplanner.data.entity.HomeworkEntity;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public abstract class HomeworkDao {

    @Insert
    public abstract void insert(HomeworkEntity ... homeworkEntities);

    @Query("SELECT * FROM homework WHERE date IN (:days)")
    public abstract Flowable<List<HomeworkEntity>> getHomework(String[]  days);

    @Query("SELECT * FROM homework WHERE date = :day")
    public abstract Flowable<List<HomeworkEntity>> getHomework(String day);

    @Query("SELECT * FROM homework WHERE date = :day AND number_in_day = :numberInDay")
    public abstract List<HomeworkEntity> getHomework(String day, int numberInDay);

    @Query("UPDATE homework SET text = :text, done = :done WHERE date = :day AND number_in_day = :numberInDay")
    public abstract int update(String day, int numberInDay, String text, boolean done);

    public void insertOrUpdate(String day, int numberInDay, String text, boolean done) {
        if(getHomework(day, numberInDay).size() > 0) {
            update(day, numberInDay, text, done);
        } else {
            HomeworkEntity entity = new HomeworkEntity();
            entity.setDate(day);
            entity.setNumber_in_day(numberInDay);
            entity.setText(text);
            entity.setDone(done);
            insert(entity);
        }
    }

}
