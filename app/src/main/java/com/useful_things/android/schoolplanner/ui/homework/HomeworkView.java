package com.useful_things.android.schoolplanner.ui.homework;

import com.useful_things.android.schoolplanner.ui.base.BaseFragmentMVP;

public abstract class HomeworkView extends BaseFragmentMVP<HomeworkPresenter> {

    public abstract String getHomework();
    public abstract void finishActivity();
    public abstract boolean isDone();

}
