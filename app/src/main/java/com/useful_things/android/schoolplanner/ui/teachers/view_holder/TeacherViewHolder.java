package com.useful_things.android.schoolplanner.ui.teachers.view_holder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.useful_things.android.schoolplanner.databinding.TeacherEntryBinding;
import com.useful_things.android.schoolplanner.ui.subjects.entity.Subject;
import com.useful_things.android.schoolplanner.ui.teachers.TeachersPresenter;
import com.useful_things.android.schoolplanner.ui.teachers.entity.Teacher;

public class TeacherViewHolder extends RecyclerView.ViewHolder {

    private TeacherEntryBinding binding;

    public TeacherViewHolder(@NonNull TeacherEntryBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void setData(Teacher teacher) {
        binding.teacherName.setText(String.format("%s %s %s",
                teacher.getLastName(), teacher.getFirstName(), teacher.getMiddleName()));

        if(teacher.getSubjects().isEmpty() && teacher.getAuditory().isEmpty()) {
            binding.teacherInfo.setVisibility(View.GONE);
        } else {
            binding.teacherInfo.setVisibility(View.VISIBLE);
            StringBuilder subjectsStr  = new StringBuilder();
            for(int i = 0;i < teacher.getSubjects().size();i++) {
                Subject subject = teacher.getSubjects().get(i);
                if(i == teacher.getSubjects().size() - 1)
                    subjectsStr.append(subject.getSubject_name());
                else
                    subjectsStr.append(String.format("%s, ", subject.getSubject_name()));
            }
            String auditory = teacher.getAuditory();
            String subjects = subjectsStr.toString();

            if (!auditory.isEmpty() && !subjects.isEmpty())
                binding.teacherInfo.setText(String.format("%s, %s", subjects, auditory));
            else
            if (auditory.isEmpty())
                binding.teacherInfo.setText(subjects);
            else
            if (subjects.isEmpty())
                binding.teacherInfo.setText(auditory);
        }
    }

    public void setPresenter(TeachersPresenter presenter, String hash){
        binding.teacherEntry.setOnClickListener(v -> presenter.openTeacher(hash));
    }

}
