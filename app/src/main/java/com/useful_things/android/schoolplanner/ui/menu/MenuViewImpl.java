package com.useful_things.android.schoolplanner.ui.menu;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.databinding.MenuBinding;
import com.useful_things.android.schoolplanner.ui.settings.SettingsViewImpl;

public class MenuViewImpl extends MenuView {


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        presenter = new MenuPresenterImpl();

        MenuBinding binding = DataBindingUtil.inflate(inflater, R.layout.menu, container, false);
        binding.menuSettings.setOnClickListener(v -> openSettings());

        return binding.getRoot();
    }

    public void openSettings() {
        Intent intent = new Intent(getActivity(), SettingsViewImpl.class);
        startActivity(intent);
    }
}
