package com.useful_things.android.schoolplanner.data.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import com.useful_things.android.schoolplanner.data.entity.SubjectEntity;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public abstract class SubjectDao {

    @Query("SELECT * FROM subjects")
    public abstract Flowable<List<SubjectEntity>> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(SubjectEntity ... subjectEntities);

    @Query("SELECT * FROM subjects WHERE subject_key = :subjectKey")
    public abstract List<SubjectEntity> getSubject(String subjectKey);

    @Query("UPDATE subjects SET subject_name = :subjectName, auditory = :auditory WHERE subject_key = :subjectKey")
    public abstract void update(String subjectKey, String subjectName, String auditory);

    @Transaction
    public void insertOrUpdate(String subjectKey, String subjectName, String auditory) {
        if(getSubject(subjectKey).size() > 0) {
            update(subjectKey, subjectName, auditory);
        } else {
            SubjectEntity entity = new SubjectEntity();
            entity.setSubject_key(subjectKey);
            entity.setSubject_name(subjectName);
            entity.setAuditory(auditory);
            insert(entity);
        }
    }

}
