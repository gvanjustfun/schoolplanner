package com.useful_things.android.schoolplanner.ui.teachers;

import com.useful_things.android.schoolplanner.RandomString;
import com.useful_things.android.schoolplanner.data.entity.SubjectEntity;
import com.useful_things.android.schoolplanner.data.entity.TeacherEntity;
import com.useful_things.android.schoolplanner.data.entity.TeacherSubjectsEntity;
import com.useful_things.android.schoolplanner.map.TeacherMapper;
import com.useful_things.android.schoolplanner.ui.base.App;
import com.useful_things.android.schoolplanner.ui.subjects.entity.Subject;
import com.useful_things.android.schoolplanner.ui.teachers.entity.Teacher;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class AddTeacherPresenterImpl extends AddTeacherPresenter {

    private String hash = "";
    private final List<Subject> subjects = new ArrayList<>();

    @Override
    public void getTeacher(String hash) {
        this.hash = hash;
        Observable<List<TeacherEntity>> teacherObservable = App.getApp().getDb().teacherDao().getTeacher(hash)
                .toObservable()
                .subscribeOn(Schedulers.io());
        Observable<List<TeacherSubjectsEntity>> teacherSubjectsObservable = App.getApp().getDb().teacherSubjectsDao().getSubjects(hash)
                .toObservable()
                .subscribeOn(Schedulers.io());
        Observable<List<SubjectEntity>> subjectObservable = App.getApp().getDb().subjectDao().getAll()
                .toObservable()
                .subscribeOn(Schedulers.io());

        Disposable disposable = Observable.zip(teacherObservable, teacherSubjectsObservable, subjectObservable, new TeacherMapper())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(entities -> {
                    if(view == null) return;
                    view.hideProgress();

                    if(entities.size() > 0) {
                        Teacher teacher = entities.get(0);
                        view.setTeacher(teacher.getLastName(),
                                teacher.getFirstName(),
                                teacher.getMiddleName(),
                                teacher.getBirthday(),
                                teacher.getAuditory(),
                                teacher.getPhone());
                        subjects.clear();
                        subjects.addAll(teacher.getSubjects());
                        view.setComposeSubjects(subjects);
                    }
                }, throwable -> {
                    if(view == null) return;
                    view.hideProgress();
                });
        compositeDisposable.add(disposable);
    }

    @Override
    public void addTeacher(String lastName, String firstName, String middleName, String birthday, String auditory, String phone) {
        if(hash.isEmpty()) {
            TeacherEntity teacherEntity = new TeacherEntity();
            hash = new RandomString().nextString();
            teacherEntity.setHash(hash);
            teacherEntity.setLastName(lastName);
            teacherEntity.setFirstName(firstName);
            teacherEntity.setMiddleName(middleName);
            teacherEntity.setAuditory(auditory);
            teacherEntity.setPhone(phone);
            teacherEntity.setBirthday(birthday);

            Completable insertTeacher = Completable.fromAction(() ->
                    App.getApp().getDb().teacherDao().insert(teacherEntity))
                    .subscribeOn(Schedulers.io());

            Disposable disposable = Completable.mergeArray(insertTeacher, getTeacherSubjectCompletable(hash))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(() -> {
                        if (view == null) return;

                        view.finishWithResult();
                    }, throwable -> {
                        if (view == null) return;

                        view.finishWithResult();
                    });
            compositeDisposable.add(disposable);
        } else {
            Completable updateTeacher = Completable.fromAction(() ->
                    App.getApp().getDb().teacherDao().update(hash, firstName, lastName, middleName, phone, auditory, birthday))
                    .subscribeOn(Schedulers.io());

            Disposable disposable = Completable.mergeArray(updateTeacher, getTeacherSubjectCompletable(hash))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(() -> {
                        if (view == null) return;

                        view.finishWithResult();
                    }, throwable -> {
                        if (view == null) return;

                        view.finishWithResult();
                    });
            compositeDisposable.add(disposable);
        }
    }

    private Completable getTeacherSubjectCompletable(String hash) {
        TeacherSubjectsEntity[] teacherSubjects = new TeacherSubjectsEntity[subjects.size()];
        for(int i = 0;i < subjects.size();i++) {
            Subject subject = subjects.get(i);
            TeacherSubjectsEntity teacherSubject = new TeacherSubjectsEntity();
            teacherSubject.setTeacher_hash(hash);
            teacherSubject.setSubject_key(subject.getSubject_key());
            teacherSubjects[i] = teacherSubject;
        }

        return Completable.fromAction(() ->
                App.getApp().getDb().teacherSubjectsDao().deleteInsert(hash, teacherSubjects))
                .subscribeOn(Schedulers.io());
    }

    @Override
    public void addComposeSubject(String key, String name) {
        Subject subject = new Subject();
        subject.setSubject_key(key);
        subject.setSubject_name(name);
        subjects.add(subject);
        view.setComposeSubjects(subjects);
    }

    @Override
    public void removeSubject(String key) {
        for(int i = 0;i < subjects.size();i++) {
            Subject subject = subjects.get(i);
            if(subject.getSubject_key().equals(key)) {
                subjects.remove(i);
                break;
            }
        }
        view.setComposeSubjects(subjects);
    }

    @Override
    public List<Subject> getSubjects() {
        return subjects;
    }
}
