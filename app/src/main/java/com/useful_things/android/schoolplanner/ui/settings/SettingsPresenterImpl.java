package com.useful_things.android.schoolplanner.ui.settings;

import com.useful_things.android.schoolplanner.Const;

public class SettingsPresenterImpl extends SettingsPresenter {

    private int[] themes = {Const.THEME_LIGHT, Const.THEME_DARK};
    private int theme = Const.THEME_LIGHT;

    @Override
    protected void onChooseTheme() {
        if(view != null)
            view.openTheme(themes);
    }

    @Override
    protected void setTheme(int theme) {
        this.theme = theme;
    }

    public int getTheme() {
        return theme;
    }
}
