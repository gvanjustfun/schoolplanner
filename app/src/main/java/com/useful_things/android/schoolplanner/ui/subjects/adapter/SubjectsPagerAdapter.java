package com.useful_things.android.schoolplanner.ui.subjects.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.useful_things.android.schoolplanner.Const;
import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.ui.subjects.SubjectsPresenter;
import com.useful_things.android.schoolplanner.ui.subjects.SubjectsViewImpl;

import java.util.ArrayList;
import java.util.List;

public class SubjectsPagerAdapter extends FragmentPagerAdapter {

    private final List<String> titles = new ArrayList<>();

    public SubjectsPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        titles.clear();
        titles.add(context.getString(R.string.my_subjects));
        titles.add(context.getString(R.string.all_subjects));
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:{
                Bundle arguments = new Bundle();
                arguments.putInt(Const.MODE, SubjectsPresenter.SUBJECTS_MODE_MY);
                Fragment fragment = new SubjectsViewImpl();
                fragment.setArguments(arguments);
                return fragment;
            }
            case 1:{
                Bundle arguments = new Bundle();
                arguments.putInt(Const.MODE, SubjectsPresenter.SUBJECTS_MODE_ALL);
                Fragment fragment = new SubjectsViewImpl();
                fragment.setArguments(arguments);
                return fragment;
            }
        }
        return null;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if(position < titles.size()) return titles.get(position);
        return super.getPageTitle(position);
    }

    @Override
    public int getCount() {
        return titles.size();
    }
}
