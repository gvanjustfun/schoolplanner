package com.useful_things.android.schoolplanner.ui.subjects.view_holder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.Utils;
import com.useful_things.android.schoolplanner.databinding.SubjectEntryBinding;
import com.useful_things.android.schoolplanner.ui.subjects.SubjectsPresenter;
import com.useful_things.android.schoolplanner.ui.subjects.entity.Subject;

import java.util.List;

public class SubjectsViewHolder extends RecyclerView.ViewHolder {

    private SubjectEntryBinding binding;

    public SubjectsViewHolder(@NonNull SubjectEntryBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void setSubject(Subject subject, String letter, SubjectsPresenter presenter) {
        binding.subjectEntryLetter.setText(letter);
        binding.subjectEntryName.setText(subject.getSubject_name());
        binding.subjectEntry.setTag(R.id.KEY, subject.getSubject_key());
        binding.subjectEntry.setOnClickListener(v -> presenter.openSubject(subject.getSubject_key()));

        if(subject.getDaysOfWeek().size() > 0) {
            StringBuilder daysStr = new StringBuilder();
            List<Integer> days = subject.getDaysOfWeek();
            for(int i = 0;i < days.size();i++) {
                int dayRes = Utils.getDayOfWeekName(days.get(i));
                String dayStr = binding.getRoot().getContext().getString(dayRes);
                daysStr.append(i == (days.size() - 1) ?
                        dayStr :
                        String.format("%s, ", dayStr));
            }
            binding.subjectEntryDays.setText(daysStr.toString());
            binding.subjectEntryDays.setVisibility(View.VISIBLE);
        } else {
            binding.subjectEntryDays.setVisibility(View.GONE);
        }

        if(subject.getTeachers().size() > 0) {
            StringBuilder infoStr = new StringBuilder();
            List<String> teachers = subject.getTeachers();
            for(int i = 0;i < teachers.size();i++) {
                infoStr.append(i == (teachers.size() - 1) ?
                        teachers.get(i) :
                        String.format("%s, ", teachers.get(i)));
            }
            binding.subjectEntryInfo.setText(infoStr.toString());
            binding.subjectEntryInfo.setVisibility(View.VISIBLE);
        } else {
            binding.subjectEntryInfo.setVisibility(View.GONE);
        }
    }

}
