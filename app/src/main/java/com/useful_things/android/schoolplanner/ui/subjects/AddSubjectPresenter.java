package com.useful_things.android.schoolplanner.ui.subjects;

import com.useful_things.android.schoolplanner.ui.base.BaseFragmentPresenter;

public abstract class AddSubjectPresenter extends BaseFragmentPresenter<AddSubjectView> {

    public abstract void addSubject(String subjectName, String auditory);

}
