package com.useful_things.android.schoolplanner.ui.base;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.databinding.BaseFrameBinding;

public abstract class BaseActivityMVP<E extends ViewDataBinding, T extends BaseActivityPresenter> extends BaseActivity {

    protected T presenter;
    protected E binding;
    protected BaseFrameBinding baseBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        baseBinding = DataBindingUtil.setContentView(this, R.layout.base_frame);
        binding = DataBindingUtil.inflate(getLayoutInflater(), getViewId(), baseBinding.fragmentFrame, false);
        baseBinding.fragmentFrame.addView(binding.getRoot());

        initView();
        presenter.onAttach(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDetach();
    }

    protected abstract int getViewId();
    protected abstract void initView();
    protected abstract void hideProgress();

}
