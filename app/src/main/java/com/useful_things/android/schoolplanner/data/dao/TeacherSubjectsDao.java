package com.useful_things.android.schoolplanner.data.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import com.useful_things.android.schoolplanner.data.entity.TeacherSubjectsEntity;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public abstract class TeacherSubjectsDao {

    @Query("SELECT * FROM teacher_subjects")
    public abstract Flowable<List<TeacherSubjectsEntity>> getAll();

    @Query("SELECT * FROM teacher_subjects WHERE teacher_hash = :teacherHash")
    public abstract Flowable<List<TeacherSubjectsEntity>> getSubjects(String teacherHash);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(TeacherSubjectsEntity ... teacherSubjectsEntities);

    @Query("DELETE FROM teacher_subjects WHERE teacher_hash = :teacherHash")
    public abstract void delete(String teacherHash);

    @Transaction
    public void deleteInsert(String teacherHash, TeacherSubjectsEntity... teacherSubjectsEntities) {
        delete(teacherHash);
        insert(teacherSubjectsEntities);
    }

}
