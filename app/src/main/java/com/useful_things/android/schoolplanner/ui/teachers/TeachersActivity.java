package com.useful_things.android.schoolplanner.ui.teachers;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.useful_things.android.schoolplanner.Const;
import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.databinding.BaseFrameBinding;
import com.useful_things.android.schoolplanner.ui.base.BaseActivity;

public class TeachersActivity extends BaseActivity {

    private BaseFrameBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.base_frame);
        Fragment fragment = new TeachersViewImpl();
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_frame, fragment).commit();
        binding.toolbar.toolbarTitle.setText(R.string.teachers);
        binding.toolbar.toolbarAction1.setImageResource(R.drawable.ic_plus);
        binding.toolbar.toolbarAction1.setVisibility(View.VISIBLE);
        binding.toolbar.toolbarAction1.setOnClickListener(v -> openAddTeacher());
    }

    @Override
    protected void onStart() {
        super.onStart();
        binding.navigationBottom.setSelectedItemId(R.id.navigation_teacher);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case Const.CREATE_TEACHER_CODE:{
                if(resultCode == RESULT_OK) {
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_frame);
                    if(fragment != null && fragment instanceof TeachersView) {
                        ((TeachersView)fragment).loadTeachers();
                    }
                }
                break;
            }
        }
    }

    private void openAddTeacher(){
        Intent intent = new Intent(this, AddTeacherActivity.class);
        startActivityForResult(intent, Const.CREATE_TEACHER_CODE);
    }

}
