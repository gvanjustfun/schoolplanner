package com.useful_things.android.schoolplanner.ui.subjects;

import com.useful_things.android.schoolplanner.data.entity.ScheduleEntity;
import com.useful_things.android.schoolplanner.data.entity.SubjectEntity;
import com.useful_things.android.schoolplanner.data.entity.TeacherEntity;
import com.useful_things.android.schoolplanner.data.entity.TeacherSubjectsEntity;
import com.useful_things.android.schoolplanner.map.SubjectsAllMapper;
import com.useful_things.android.schoolplanner.map.SubjectsMyMapper;
import com.useful_things.android.schoolplanner.ui.base.App;
import com.useful_things.android.schoolplanner.ui.subjects.entity.Subject;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function4;
import io.reactivex.schedulers.Schedulers;

public class SubjectsPresenterImpl extends SubjectsPresenter {


    public SubjectsPresenterImpl(int mode) {
        super(mode);
    }

    @Override
    public void loadSubjects() {
        Observable<List<SubjectEntity>> subjectObservable = App.getApp().getDb().subjectDao().getAll()
                .toObservable()
                .subscribeOn(Schedulers.io());
        Observable<List<ScheduleEntity>> scheduleObservable = App.getApp().getDb().scheduleDao().getSchedule(App.getApp().getSemId())
                .toObservable()
                .subscribeOn(Schedulers.io());
        Observable<List<TeacherEntity>> teacherObservable = App.getApp().getDb().teacherDao().getAll()
                .toObservable()
                .subscribeOn(Schedulers.io());
        Observable<List<TeacherSubjectsEntity>> teacherSubjectsObservable = App.getApp().getDb().teacherSubjectsDao().getAll()
                .toObservable()
                .subscribeOn(Schedulers.io());

        Observable.zip(subjectObservable, scheduleObservable, teacherObservable, teacherSubjectsObservable, getMapper())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Subject>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(List<Subject> subjects) {
                        if(view == null) return;
                        view.hideProgress();
                        if(subjects != null) {
                            view.setSubjects(subjects);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void openSubject(String subjectKey) {

    }

    private Function4<List<SubjectEntity>, List<ScheduleEntity>, List<TeacherEntity>, List<TeacherSubjectsEntity>, List<Subject>> getMapper() {
        return mode == SUBJECTS_MODE_MY ? new SubjectsMyMapper() : new SubjectsAllMapper();
    }
}
