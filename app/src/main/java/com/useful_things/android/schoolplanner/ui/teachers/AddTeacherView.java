package com.useful_things.android.schoolplanner.ui.teachers;

import com.useful_things.android.schoolplanner.ui.base.BaseFragmentMVP;
import com.useful_things.android.schoolplanner.ui.subjects.entity.Subject;

import java.util.List;

public abstract class AddTeacherView extends BaseFragmentMVP<AddTeacherPresenter> {

    public abstract void hideProgress();
    public abstract void setTeacher(String lastName,
                                    String firstName,
                                    String middleName,
                                    String birthday,
                                    String auditory,
                                    String phone);
    public abstract void finishWithResult();
    public abstract void addComposeSubject(String subjectKey, String name);
    public abstract void setComposeSubjects(List<Subject> subjects);

}
