package com.useful_things.android.schoolplanner.ui.scheduler;

import com.useful_things.android.schoolplanner.Const;
import com.useful_things.android.schoolplanner.data.entity.HomeworkEntity;
import com.useful_things.android.schoolplanner.data.entity.ScheduleEntity;
import com.useful_things.android.schoolplanner.data.entity.SubjectEntity;
import com.useful_things.android.schoolplanner.map.ScheduleHomeworkMapper;
import com.useful_things.android.schoolplanner.ui.base.App;
import com.useful_things.android.schoolplanner.ui.scheduler.entity.Schedule;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SchedulePresenterImpl extends SchedulePresenter {

    private String firstDate = "";
    private final String[] days = new String[SchedulePresenter.WEEK_SIZE];

    public SchedulePresenterImpl(int weekOfYear) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.WEEK_OF_YEAR, weekOfYear);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        int firstDay = calendar.get(Calendar.DAY_OF_YEAR);
        SimpleDateFormat dateFormat = new SimpleDateFormat(Const.DD_MMM_YYYY);
        firstDate = dateFormat.format(calendar.getTime());
        for(int i = 0;i < SchedulePresenter.WEEK_SIZE;i++) {
            calendar.set(Calendar.DAY_OF_YEAR, firstDay + i);
            days[i] = dateFormat.format(calendar.getTime());
        }
    }

    @Override
    public void loadSchedule() {
        Observable<List<ScheduleEntity>> scheduleObservable = App.getApp().getDb().scheduleDao().getSchedule(App.getApp().getSemId())
                .toObservable()
                .subscribeOn(Schedulers.io());
        Observable<List<HomeworkEntity>> homeworkObservable = App.getApp().getDb().homeworkDao().getHomework(days)
                .toObservable()
                .subscribeOn(Schedulers.io());
        Observable<List<SubjectEntity>> subjectObservable = App.getApp().getDb().subjectDao().getAll()
                .toObservable()
                .subscribeOn(Schedulers.io());
        Observable<String> dateObservable = Observable.just(firstDate);
        Disposable disposable = Observable.zip(scheduleObservable, homeworkObservable, subjectObservable, dateObservable, new ScheduleHomeworkMapper())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(schedulesRes -> {
                    if(view == null) return;
                    view.hideProgress();

                    view.setSchedule(schedulesRes);
                }, throwable -> throwable.printStackTrace());
        compositeDisposable.add(disposable);
    }

    @Override
    public void openSubjectsList(int dayOfWeek, int numberInDay) {
        if(view != null)
            view.openSubjectsList(dayOfWeek, numberInDay);
    }

    @Override
    public void addSubject(int dayOfWeek, int numberInDay, String key) {
        Disposable disposable = Completable
                .fromAction(() -> App.getApp().getDb().scheduleDao()
                        .insertOrUpdate(App.getApp().getSemId(), dayOfWeek, numberInDay, key))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    if(view == null) return;
                    loadSchedule();
                }, throwable -> {
                    throwable.printStackTrace();
                });
        compositeDisposable.add(disposable);
    }


    @Override
    public void openHomework(Schedule schedule) {
        if(view != null) view.openHomework(schedule);
    }
}
