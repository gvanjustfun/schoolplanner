package com.useful_things.android.schoolplanner.data.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "subjects")
public class SubjectEntity {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int subject_id;

    @ColumnInfo(name = "subject_key")
    private String subject_key;

    @ColumnInfo(name = "subject_name")
    private String subject_name;

    @ColumnInfo(name = "auditory")
    private String auditory;

    @NonNull
    public int getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(@NonNull int subject_id) {
        this.subject_id = subject_id;
    }

    public String getSubject_key() {
        return subject_key;
    }

    public void setSubject_key(String subject_key) {
        this.subject_key = subject_key;
    }

    public String getAuditory() {
        return auditory;
    }

    public void setAuditory(String auditory) {
        this.auditory = auditory;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }
}
