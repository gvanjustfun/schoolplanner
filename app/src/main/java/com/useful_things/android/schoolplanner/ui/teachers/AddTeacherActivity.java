package com.useful_things.android.schoolplanner.ui.teachers;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.useful_things.android.schoolplanner.Const;
import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.databinding.BaseFrameBinding;
import com.useful_things.android.schoolplanner.ui.base.BaseActivity;

public class AddTeacherActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        BaseFrameBinding binding = DataBindingUtil.setContentView(this, R.layout.base_frame);
        binding.toolbar.toolbarBack.setVisibility(View.VISIBLE);
        binding.toolbar.toolbarTitle.setText(R.string.add_teacher);
        binding.navigationBottom.setSelectedItemId(R.id.navigation_teacher);

        Bundle arguments = new Bundle();
        if(getIntent().hasExtra(Const.HASH)) {
            arguments.putString(Const.HASH, getIntent().getStringExtra(Const.HASH));
        }
        Fragment fragment = new AddTeacherViewImpl();
        fragment.setArguments(arguments);
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_frame, fragment).commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case Const.CHOOSE_SUBJECT_CODE:{
                if(resultCode == RESULT_OK && data != null && data.hasExtra(Const.KEY) && data.hasExtra(Const.NAME)) {
                    String key = data.getStringExtra(Const.KEY);
                    String name = data.getStringExtra(Const.NAME);
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_frame);
                    if(fragment != null && fragment instanceof AddTeacherView) {
                        ((AddTeacherView)fragment).addComposeSubject(key, name);
                    }
                }
                break;
            }
        }
    }
}
