package com.useful_things.android.schoolplanner.ui.homework.entity;

public class Homework {

    private int numberInDay;
    private String text;
    private boolean done;

    public int getNumberInDay() {
        return numberInDay;
    }

    public void setNumberInDay(int numberInDay) {
        this.numberInDay = numberInDay;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }
}
