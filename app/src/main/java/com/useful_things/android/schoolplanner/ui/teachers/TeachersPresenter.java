package com.useful_things.android.schoolplanner.ui.teachers;

import com.useful_things.android.schoolplanner.ui.base.BaseFragmentPresenter;

public abstract class TeachersPresenter extends BaseFragmentPresenter<TeachersView> {

    public abstract void loadTeachers();
    public abstract void openTeacher(String hash);

}
