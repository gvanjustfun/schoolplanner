package com.useful_things.android.schoolplanner.ui.teachers.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.Utils;
import com.useful_things.android.schoolplanner.databinding.TeacherEntryBinding;
import com.useful_things.android.schoolplanner.ui.teachers.TeachersPresenter;
import com.useful_things.android.schoolplanner.ui.teachers.view_holder.TeacherViewHolder;
import com.useful_things.android.schoolplanner.ui.teachers.entity.Teacher;

import java.util.ArrayList;
import java.util.List;

public class TeachersAdapter extends RecyclerView.Adapter<TeacherViewHolder> {

    private final List<Teacher> teachers = new ArrayList<>();
    private TeachersPresenter presenter;

    public TeachersAdapter(TeachersPresenter presenter) {
        this.presenter = presenter;
    }

    public void setTeachers(List<Teacher> teachers) {
        this.teachers.clear();
        if(teachers != null) {
            this.teachers.addAll(teachers);
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public TeacherViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        TeacherEntryBinding binding = DataBindingUtil.inflate(inflater, R.layout.teacher_entry, viewGroup, false);
        return new TeacherViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull TeacherViewHolder holder, int i) {
        Teacher teacher = teachers.get(i);
        holder.setData(teacher);
        holder.setPresenter(presenter, teacher.getHash());
    }

    @Override
    public int getItemCount() {
        return teachers.size();
    }

}
