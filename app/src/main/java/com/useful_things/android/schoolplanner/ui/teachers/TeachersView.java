package com.useful_things.android.schoolplanner.ui.teachers;

import com.useful_things.android.schoolplanner.ui.base.BaseFragmentMVP;
import com.useful_things.android.schoolplanner.ui.teachers.entity.Teacher;

import java.util.List;

public abstract class TeachersView extends BaseFragmentMVP<TeachersPresenter> {

    public abstract void hideProgress();
    public abstract void setTeachers(List<Teacher> teachers);
    public abstract void openTeacher(String hash);
    public abstract void loadTeachers();

}
