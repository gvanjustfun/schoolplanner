package com.useful_things.android.schoolplanner.ui.settings;

import com.useful_things.android.schoolplanner.databinding.SettingsBinding;
import com.useful_things.android.schoolplanner.ui.base.BaseActivityMVP;

public abstract class SettingsView extends BaseActivityMVP<SettingsBinding, SettingsPresenter> {

    protected abstract void openTheme(int[] themes);

}
