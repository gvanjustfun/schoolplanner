package com.useful_things.android.schoolplanner.ui.subjects;

import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.databinding.AddSubjectBinding;

public class AddSubjectViewImpl extends AddSubjectView {

    private AddSubjectBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        presenter = new AddSubjectPresenterImpl();

        binding = DataBindingUtil.inflate(inflater, R.layout.add_subject, container, false);
        binding.addSubjectBtn.setOnClickListener(v -> addSubject());

        return binding.getRoot();
    }

    private void addSubject(){
        String subjectName = binding.addSubjectName.getText().toString();
        if(subjectName.isEmpty()) {
            binding.addSubjectNameWrapper.setErrorEnabled(true);
            binding.addSubjectNameWrapper.setError(getString(R.string.field_is_required_to_fill));
            return;
        } else {
            binding.addSubjectNameWrapper.setErrorEnabled(false);
            binding.addSubjectNameWrapper.setError(null);
        }
        String auditory = binding.addSubjectAuditory.getText().toString();

        View focus = getActivity().getCurrentFocus();
        if(focus != null) {
            InputMethodManager manager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            manager.hideSoftInputFromWindow(focus.getWindowToken(), 0);
        }

        presenter.addSubject(subjectName, auditory);
    }

    @Override
    public void closeWithResult() {
        getActivity().setResult(Activity.RESULT_OK);
        getActivity().finish();
    }
}
