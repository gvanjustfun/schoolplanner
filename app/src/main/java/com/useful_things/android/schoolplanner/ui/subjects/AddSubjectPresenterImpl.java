package com.useful_things.android.schoolplanner.ui.subjects;

import com.useful_things.android.schoolplanner.Const;
import com.useful_things.android.schoolplanner.ui.base.App;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class AddSubjectPresenterImpl extends AddSubjectPresenter {

    @Override
    public void addSubject(String subjectName, String auditory) {
        String subjectKey = transliterateCyrillic(subjectName);
        Disposable disposable = Completable.fromAction(() ->
                App.getApp().getDb().subjectDao().insertOrUpdate(subjectKey, subjectName, auditory))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    if(view == null) return;

                    view.closeWithResult();
                }, throwable -> throwable.printStackTrace());
        compositeDisposable.add(disposable);
    }

    private String transliterateCyrillic(String src){
        char[] abcCyr =   {' ','а','б','в','г','д','е','ё', 'ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х', 'ц','ч', 'ш','щ','ъ','ы','ь','э', 'ю','я','А','Б','В','Г','Д','Е','Ё', 'Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х', 'Ц', 'Ч','Ш', 'Щ','Ъ','Ы','Ь','Э','Ю','Я','a','і','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
        String[] abcLat = {"_","a","b","v","g","d","e","e","zh","z","i","y","k","l","m","n","o","p","r","s","t","u","f","h","ts","ch","sh","sch", "","i", "","e","ju","ja","A","B","V","G","D","E","E","Zh","Z","I","Y","K","L","M","N","O","P","R","S","T","U","F","H","Ts","Ch","Sh","Sch", "","I", "","E","Ju","Ja","i","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
        StringBuilder builder = new StringBuilder();
        for(int i = 0;i < src.length();i++) {
            for(int j = 0; j < abcCyr.length;j++) {
                if(src.charAt(i) == abcCyr[j]) {
                    builder.append(abcLat[j]);
                }
            }
        }
        return String.format("%s_%s", Const.CUSTOM, builder.toString());
    }

}
