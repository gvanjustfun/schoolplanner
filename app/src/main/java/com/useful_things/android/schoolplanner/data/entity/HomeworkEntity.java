package com.useful_things.android.schoolplanner.data.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "homework")
public class HomeworkEntity {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int homework_id;
    @ColumnInfo(name = "date")
    private String date;
    @ColumnInfo(name = "number_in_day")
    private int number_in_day;
    @ColumnInfo(name = "text")
    private String text;
    @ColumnInfo(name = "done")
    private boolean done;

    @NonNull
    public int getHomework_id() {
        return homework_id;
    }

    public void setHomework_id(@NonNull int homework_id) {
        this.homework_id = homework_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public int getNumber_in_day() {
        return number_in_day;
    }

    public void setNumber_in_day(int number_in_day) {
        this.number_in_day = number_in_day;
    }
}
