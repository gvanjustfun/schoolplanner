package com.useful_things.android.schoolplanner.ui.subjects;

import com.useful_things.android.schoolplanner.ui.base.BaseFragmentMVP;
import com.useful_things.android.schoolplanner.ui.subjects.entity.Subject;

import java.util.List;

public abstract class ChooseSubjectsView extends BaseFragmentMVP<ChooseSubjectsPresenter> {

    public abstract void hideProgress();
    public abstract void setSubjects(List<Subject> subjects);
    public abstract void setResult(String key, String name);
    public abstract void loadSubjects();

}
