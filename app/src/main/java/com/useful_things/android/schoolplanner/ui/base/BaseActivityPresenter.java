package com.useful_things.android.schoolplanner.ui.base;

import io.reactivex.disposables.CompositeDisposable;

public class BaseActivityPresenter<T extends BaseActivity> {

    protected T view;
    protected CompositeDisposable compositeDisposable;

    public void onAttach(T view){
        this.view = view;
        compositeDisposable = new CompositeDisposable();
    }

    public void onDetach() {
        view = null;
        if(compositeDisposable != null) {
            compositeDisposable.clear();
        }
    }

}
