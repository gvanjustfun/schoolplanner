package com.useful_things.android.schoolplanner.ui.scheduler.entity;

public class Schedule {

    private String date;
    private String subjectName;
    private int dayOfWeek;
    private int numberInDay;
    private String homework;
    private boolean homeworkDone;

    public Schedule() {
        date = "";
        subjectName = "";
        homework = "";
        homeworkDone = false;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public int getNumberInDay() {
        return numberInDay;
    }

    public void setNumberInDay(int numberInDay) {
        this.numberInDay = numberInDay;
    }

    public String getHomework() {
        return homework;
    }

    public void setHomework(String homework) {
        this.homework = homework;
    }

    public boolean isHomeworkDone() {
        return homeworkDone;
    }

    public void setHomeworkDone(boolean homeworkDone) {
        this.homeworkDone = homeworkDone;
    }
}
