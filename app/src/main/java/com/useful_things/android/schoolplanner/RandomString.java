package com.useful_things.android.schoolplanner;

import java.security.SecureRandom;
import java.util.Random;

public class RandomString {

    private static final String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String lower = upper.toLowerCase();
    private static final String digits = "0123456789";
    private static final String alphanum = upper + lower + digits;

    private final Random random;
    private final char[] symbols;
    private final char[] buf;

    public RandomString() {
        this(16);
    }

    public RandomString(int length) {
        this.random = new SecureRandom();
        symbols = alphanum.toCharArray();
        buf = new char[length];
    }

    public String nextString(){
        for(int i = 0;i < buf.length;i++) {
            buf[i] = symbols[random.nextInt(symbols.length)];
        }
        return new String(buf);
    }

}
