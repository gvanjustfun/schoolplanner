package com.useful_things.android.schoolplanner.data.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "teacher_subjects")
public class TeacherSubjectsEntity {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    private int teacher_subject_id;

    @ColumnInfo(name = "teacher_hash")
    private String teacher_hash;

    @ColumnInfo(name =  "subject_key")
    private String subject_key;

    @NonNull
    public int getTeacher_subject_id() {
        return teacher_subject_id;
    }

    public void setTeacher_subject_id(@NonNull int teacher_subject_id) {
        this.teacher_subject_id = teacher_subject_id;
    }

    public String getTeacher_hash() {
        return teacher_hash;
    }

    public void setTeacher_hash(String teacher_hash) {
        this.teacher_hash = teacher_hash;
    }

    public String getSubject_key() {
        return subject_key;
    }

    public void setSubject_key(String subject_key) {
        this.subject_key = subject_key;
    }
}
