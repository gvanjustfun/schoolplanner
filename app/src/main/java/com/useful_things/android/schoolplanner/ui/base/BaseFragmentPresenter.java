package com.useful_things.android.schoolplanner.ui.base;

import io.reactivex.disposables.CompositeDisposable;

public abstract class BaseFragmentPresenter<T extends BaseFragment> {

    protected T view;
    protected CompositeDisposable compositeDisposable;

    public BaseFragmentPresenter() {
        compositeDisposable = new CompositeDisposable();
    }

    public void onAttach(T view) {
        this.view = view;
    }

    public void onDetach() {
        view = null;
        if(compositeDisposable != null)
            compositeDisposable.clear();
    }

}
