package com.useful_things.android.schoolplanner.ui.scheduler.view_holder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.useful_things.android.schoolplanner.Const;
import com.useful_things.android.schoolplanner.databinding.ScheduleHeaderBinding;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ScheduleHeaderViewHolder extends RecyclerView.ViewHolder {

    private ScheduleHeaderBinding binding;

    public ScheduleHeaderViewHolder(@NonNull ScheduleHeaderBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void setDate(String dateStr){
        SimpleDateFormat inputFormat = new SimpleDateFormat(Const.DD_MMM_YYYY);
        SimpleDateFormat outputFormat = new SimpleDateFormat(Const.EEEE_DD_MMMM);
        Calendar calendar = Calendar.getInstance();
        String todayStr = inputFormat.format(calendar.getTime());
        try {
            Date date = inputFormat.parse(dateStr);
            binding.scheduleHeaderDate.setText(outputFormat.format(date));
            if(todayStr.equalsIgnoreCase(dateStr)) {
                binding.scheduleHeaderToday.setVisibility(View.VISIBLE);
            } else {
                binding.scheduleHeaderToday.setVisibility(View.GONE);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}
