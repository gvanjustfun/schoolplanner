package com.useful_things.android.schoolplanner.ui.subjects;

import com.useful_things.android.schoolplanner.map.SubjectMapper;
import com.useful_things.android.schoolplanner.ui.base.App;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public class ChooseSubjectsPresenterImpl extends ChooseSubjectsPresenter {


    @Override
    public void loadData() {
        Disposable disposable = App.getApp().getDb().subjectDao().getAll()
                .map(new SubjectMapper())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(resSubjects -> {
                    if(view.isClosing()) return;
                    view.hideProgress();
                    view.setSubjects(resSubjects);
                }, throwable -> view.hideProgress());
        compositeDisposable.add(disposable);
    }

    @Override
    public void openSubject(String key, String name) {
        view.setResult(key, name);
    }
}
