package com.useful_things.android.schoolplanner.ui.teachers;

import com.useful_things.android.schoolplanner.ui.subjects.entity.Subject;

import java.util.List;

public interface ComposeSubjectsPresenter {

    void removeSubject(String key);
    List<Subject> getSubjects();

}
