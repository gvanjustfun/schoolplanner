package com.useful_things.android.schoolplanner.ui.settings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;

import com.useful_things.android.schoolplanner.Const;
import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.ui.menu.MenuActivity;

public class SettingsViewImpl extends SettingsView {

    @Override
    protected int getViewId() {
        return R.layout.settings;
    }

    @Override
    protected void initView() {
        presenter = new SettingsPresenterImpl();

        baseBinding.toolbar.toolbarTitle.setText(R.string.settings);
        baseBinding.toolbar.toolbarBack.setVisibility(View.VISIBLE);
        baseBinding.navigationBottom.setSelectedItemId(R.id.navigation_menu);

        SharedPreferences prefs = getSharedPreferences(Const.SETTINGS_PREFERENCES, MODE_PRIVATE);
        int theme = prefs.getInt(Const.APP_THEME, Const.THEME_LIGHT);
        presenter.setTheme(theme);

        binding.settingsTheme.setOnClickListener(v -> presenter.onChooseTheme());
        binding.settingsThemeValue.setText(getThemeName(theme));
    }

    @Override
    protected void hideProgress() {

    }

    @Override
    protected void openTheme(int[] themes) {
        PopupMenu popupMenu = new PopupMenu(this, binding.settingsTheme, GravityCompat.START);
        for(int i = 0;i < themes.length;i++) {
            popupMenu.getMenu().add(getThemeName(themes[i]));
            popupMenu.getMenu().getItem(i).setCheckable(true);
            popupMenu.getMenu().getItem(i).setChecked(presenter.getTheme() == themes[i]);
        }
        popupMenu.show();
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                for(int i = 0;i < themes.length;i++) {
                    if(getThemeName(themes[i]).equals(menuItem.getTitle().toString())) {
                        SharedPreferences prefs = getSharedPreferences(Const.SETTINGS_PREFERENCES, MODE_PRIVATE);
                        prefs.edit().putInt(Const.APP_THEME, themes[i]).apply();

                        Intent intent = new Intent(SettingsViewImpl.this, MenuActivity.class);
                        finish();
                        startActivity(intent);
                        startActivity(getIntent());
                    }
                }
                return false;
            }
        });
    }

    private String getThemeName(int theme) {
        switch (theme) {
            case Const.THEME_LIGHT: return getString(R.string.light);
            case Const.THEME_DARK: return getString(R.string.dark);
        }
        return "";
    }

}
