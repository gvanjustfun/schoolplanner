package com.useful_things.android.schoolplanner.ui.homework;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.useful_things.android.schoolplanner.Const;
import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.databinding.BaseFrameBinding;
import com.useful_things.android.schoolplanner.ui.base.BaseActivity;

public class HomeworkActivity extends BaseActivity {


    private BaseFrameBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.base_frame);
        binding.navigationBottom.setSelectedItemId(R.id.navigation_schedule);
        binding.toolbar.toolbarBack.setVisibility(View.VISIBLE);
        binding.toolbar.toolbarTitle.setText(R.string.homework);

        String dateStr = getIntent().getStringExtra(Const.DATE);
        int numberIdDay = getIntent().getIntExtra(Const.NUMBER_IN_DAY, 0);
        String homeworkText = getIntent().getStringExtra(Const.HOMEWORK_TEXT);

        Bundle bundle = new Bundle();
        bundle.putString(Const.DATE, dateStr);
        bundle.putInt(Const.NUMBER_IN_DAY, numberIdDay);
        bundle.putString(Const.HOMEWORK_TEXT, homeworkText);
        Fragment fragment = new HomeworkViewImpl();
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_frame, fragment).commit();
    }
}
