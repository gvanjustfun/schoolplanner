package com.useful_things.android.schoolplanner.ui.subjects;

import com.useful_things.android.schoolplanner.ui.base.BaseFragmentPresenter;

public abstract class SubjectsPresenter extends BaseFragmentPresenter<SubjectsView> {

    public static final int SUBJECTS_MODE_MY = 0;
    public static final int SUBJECTS_MODE_ALL = 1;
    protected int mode;

    public SubjectsPresenter(int mode) {
        this.mode = mode;
    }

    public abstract void loadSubjects();
    public abstract void openSubject(String subjectKey);

}
