package com.useful_things.android.schoolplanner.ui.subjects;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.useful_things.android.schoolplanner.Const;
import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.databinding.RecyclerBinding;
import com.useful_things.android.schoolplanner.ui.subjects.adapter.SubjectsAdapter;
import com.useful_things.android.schoolplanner.ui.subjects.entity.Subject;

import java.util.List;

public class SubjectsViewImpl extends SubjectsView {

    private RecyclerBinding binding;
    private SubjectsAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        int mode = getArguments().getInt(Const.MODE, 0);
        presenter = new SubjectsPresenterImpl(mode);
        adapter = new SubjectsAdapter(presenter);

        binding = DataBindingUtil.inflate(inflater, R.layout.recycler, container, false);
        binding.recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recycler.setAdapter(adapter);
        binding.swipeRefresh.setColorSchemeResources(R.color.colorPrimary);
        binding.swipeRefresh.setOnRefreshListener(() -> presenter.loadSubjects());

        binding.progress.progress.setVisibility(View.VISIBLE);
        presenter.loadSubjects();

        return binding.getRoot();
    }

    @Override
    public void setSubjects(List<Subject> subjects) {
        adapter.setSubjects(subjects);
    }

    @Override
    public void hideProgress() {
        binding.progress.progress.setVisibility(View.GONE);
        binding.swipeRefresh.setRefreshing(false);
    }
}
