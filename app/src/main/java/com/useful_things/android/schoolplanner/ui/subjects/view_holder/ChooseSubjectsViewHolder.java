package com.useful_things.android.schoolplanner.ui.subjects.view_holder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.Utils;
import com.useful_things.android.schoolplanner.databinding.ChooseSubjectEntryBinding;
import com.useful_things.android.schoolplanner.databinding.SubjectEntryBinding;
import com.useful_things.android.schoolplanner.ui.subjects.ChooseSubjectsPresenter;
import com.useful_things.android.schoolplanner.ui.subjects.entity.Subject;

public class ChooseSubjectsViewHolder extends RecyclerView.ViewHolder {

    private ChooseSubjectEntryBinding binding;

    public ChooseSubjectsViewHolder(@NonNull ChooseSubjectEntryBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void setClickListener(View.OnClickListener onClickListener){
        binding.subjectEntry.setOnClickListener(onClickListener);
    }

    public void setSubject(Subject subject, String letter, ChooseSubjectsPresenter presenter) {
        binding.subjectEntryLetter.setText(letter);
        binding.subjectEntryName.setText(subject.getSubject_name());
        binding.subjectEntry.setOnClickListener(v ->
                presenter.openSubject(subject.getSubject_key(), subject.getSubject_name()));
    }

}
