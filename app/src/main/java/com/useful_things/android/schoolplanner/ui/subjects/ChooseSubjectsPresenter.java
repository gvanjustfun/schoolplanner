package com.useful_things.android.schoolplanner.ui.subjects;

import com.useful_things.android.schoolplanner.ui.base.BaseFragmentPresenter;

public abstract class ChooseSubjectsPresenter extends BaseFragmentPresenter<ChooseSubjectsView> {

    public abstract void loadData();
    public abstract void openSubject(String key, String name);

}
