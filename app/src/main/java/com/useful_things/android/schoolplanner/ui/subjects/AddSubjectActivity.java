package com.useful_things.android.schoolplanner.ui.subjects;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.databinding.BaseFrameBinding;
import com.useful_things.android.schoolplanner.ui.base.BaseActivity;

public class AddSubjectActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        BaseFrameBinding binding = DataBindingUtil.setContentView(this, R.layout.base_frame);
        binding.toolbar.toolbarBack.setVisibility(View.VISIBLE);
        binding.toolbar.toolbarTitle.setText(R.string.add_subject);

        Fragment fragment = new AddSubjectViewImpl();
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_frame, fragment).commit();
    }
}
