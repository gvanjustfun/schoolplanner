package com.useful_things.android.schoolplanner.ui.teachers.view_holder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import com.useful_things.android.schoolplanner.databinding.ComposeSubjectBinding;
import com.useful_things.android.schoolplanner.ui.subjects.entity.Subject;
import com.useful_things.android.schoolplanner.ui.teachers.ComposeSubjectsPresenter;

public class ComposeSubjectsViewHolder extends RecyclerView.ViewHolder {

    private ComposeSubjectBinding binding;

    public ComposeSubjectsViewHolder(@NonNull ComposeSubjectBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void setSubject(Subject subject, ComposeSubjectsPresenter presenter) {
        binding.composeSubjectName.setText(subject.getSubject_name());
        binding.composeSubjectRemove.setOnClickListener(v -> presenter.removeSubject(subject.getSubject_key()));
    }

}
