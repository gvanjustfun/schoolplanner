package com.useful_things.android.schoolplanner.ui.teachers;

import com.useful_things.android.schoolplanner.ui.base.BaseFragmentPresenter;

public abstract class AddTeacherPresenter extends BaseFragmentPresenter<AddTeacherView> implements ComposeSubjectsPresenter {

    public abstract void getTeacher(String hash);
    public abstract void addTeacher(String lastName,
                                    String firstName,
                                    String middleName,
                                    String birthday,
                                    String auditory,
                                    String phone);
    public abstract void addComposeSubject(String key,
                                           String name);

}
