package com.useful_things.android.schoolplanner.ui.homework;

import com.useful_things.android.schoolplanner.ui.base.BaseFragmentPresenter;

public abstract class HomeworkPresenter extends BaseFragmentPresenter<HomeworkView> {

    protected String dateStr;
    protected int numberInDay;

    public HomeworkPresenter(String dateStr, int numberInDay) {
        this.dateStr = dateStr;
        this.numberInDay = numberInDay;
    }

    public abstract void saveHomework();

}
