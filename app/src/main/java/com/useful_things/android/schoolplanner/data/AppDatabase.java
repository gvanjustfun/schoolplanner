package com.useful_things.android.schoolplanner.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.useful_things.android.schoolplanner.data.dao.HomeworkDao;
import com.useful_things.android.schoolplanner.data.dao.ScheduleDao;
import com.useful_things.android.schoolplanner.data.dao.SubjectDao;
import com.useful_things.android.schoolplanner.data.dao.TeacherDao;
import com.useful_things.android.schoolplanner.data.dao.TeacherSubjectsDao;
import com.useful_things.android.schoolplanner.data.entity.HomeworkEntity;
import com.useful_things.android.schoolplanner.data.entity.ScheduleEntity;
import com.useful_things.android.schoolplanner.data.entity.SubjectEntity;
import com.useful_things.android.schoolplanner.data.entity.TeacherEntity;
import com.useful_things.android.schoolplanner.data.entity.TeacherSubjectsEntity;

@Database(entities = {TeacherEntity.class, SubjectEntity.class, ScheduleEntity.class, HomeworkEntity.class, TeacherSubjectsEntity.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public abstract TeacherDao teacherDao();

    public abstract SubjectDao subjectDao();

    public abstract ScheduleDao scheduleDao();

    public abstract HomeworkDao homeworkDao();

    public abstract TeacherSubjectsDao teacherSubjectsDao();

}
