package com.useful_things.android.schoolplanner.ui.subjects.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.databinding.ChooseSubjectEntryBinding;
import com.useful_things.android.schoolplanner.databinding.SubjectEntryBinding;
import com.useful_things.android.schoolplanner.ui.subjects.ChooseSubjectsPresenter;
import com.useful_things.android.schoolplanner.ui.subjects.entity.Subject;
import com.useful_things.android.schoolplanner.ui.subjects.view_holder.ChooseSubjectsViewHolder;

import java.util.ArrayList;
import java.util.List;

public class ChooseSubjectsAdapter extends RecyclerView.Adapter<ChooseSubjectsViewHolder> {

    private final List<Subject> subjects = new ArrayList<>();
    private ChooseSubjectsPresenter presenter;

    public ChooseSubjectsAdapter(ChooseSubjectsPresenter presenter) {
        this.presenter = presenter;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects.clear();
        this.subjects.addAll(subjects);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ChooseSubjectsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        ChooseSubjectEntryBinding binding = DataBindingUtil.inflate(inflater, R.layout.choose_subject_entry, viewGroup, false);
        return new ChooseSubjectsViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ChooseSubjectsViewHolder holder, int i) {
        Subject subject = subjects.get(i);
        String currLetter = subject.getSubject_name().substring(0, 1);
        String prevLetter = "";
        if(i > 0) prevLetter = subjects.get(i - 1).getSubject_name().substring(0, 1);
        String letter = currLetter.equals(prevLetter) ? "" : currLetter;
        holder.setSubject(subject, letter, presenter);
    }

    @Override
    public int getItemCount() {
        return subjects.size();
    }
}
