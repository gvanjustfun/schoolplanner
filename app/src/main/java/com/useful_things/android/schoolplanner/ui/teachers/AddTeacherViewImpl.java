package com.useful_things.android.schoolplanner.ui.teachers;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.useful_things.android.schoolplanner.Const;
import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.databinding.AddTeacherBinding;
import com.useful_things.android.schoolplanner.ui.subjects.ChooseSubjectsActivity;
import com.useful_things.android.schoolplanner.ui.subjects.entity.Subject;
import com.useful_things.android.schoolplanner.ui.teachers.adapter.ComposeSubjectsAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AddTeacherViewImpl extends AddTeacherView {

    private AddTeacherBinding binding;
    private ComposeSubjectsAdapter composeSubjectsAdapter;
    private static Date birthday = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        presenter = new AddTeacherPresenterImpl();

        binding = DataBindingUtil.inflate(inflater, R.layout.add_teacher, container, false);
        binding.teacherBirthday.setOnClickListener(v -> chooseBirthday());
        binding.teacherBirthdayText.setText(String.format("%s - %s - %s",
                getString(R.string.day), getString(R.string.month), getString(R.string.year)));
        binding.teacherAdd.setOnClickListener(v -> addTeacher());
        binding.teacherAddSubject.setOnClickListener(v -> chooseSubject());
        binding.teacherComposeSubjects.setLayoutManager(new LinearLayoutManager(getContext()));
        composeSubjectsAdapter = new ComposeSubjectsAdapter(presenter);
        binding.teacherComposeSubjects.setAdapter(composeSubjectsAdapter);

        if(getArguments() != null && getArguments().containsKey(Const.HASH)) {
            binding.progress.progress.setVisibility(View.VISIBLE);
            presenter.getTeacher(getArguments().getString(Const.HASH));
            TextView toolbarTitle = getActivity().findViewById(R.id.toolbar_title);
            toolbarTitle.setText(R.string.teacher);
            binding.teacherLastNameWrapper.setHintAnimationEnabled(false);
            binding.teacherFirstNameWrapper.setHintAnimationEnabled(false);
            binding.teacherMiddleNameWrapper.setHintAnimationEnabled(false);
            binding.teacherAuditoryWrapper.setHintAnimationEnabled(false);
            binding.teacherPhoneWrapper.setHintAnimationEnabled(false);
        }

        return binding.getRoot();
    }

    private void addTeacher(){
        String lastName = binding.teacherLastName.getText().toString();
        String firstName = binding.teacherFirstName.getText().toString();
        String middleName = binding.teacherMiddleName.getText().toString();
        String auditory = binding.teacherAuditory.getText().toString();
        String phone = binding.teacherPhone.getText().toString();

        String birthdayStr = "";
        if(birthday != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(Const.DD_MMM_YYYY);
            birthdayStr = dateFormat.format(birthday);
        }

        presenter.addTeacher(lastName, firstName, middleName, birthdayStr, auditory, phone);
    }

    private void chooseSubject(){
        Intent intent = new Intent(getActivity(), ChooseSubjectsActivity.class);
        getActivity().startActivityForResult(intent, Const.CHOOSE_SUBJECT_CODE);
    }

    public void addComposeSubject(String key, String name) {
        presenter.addComposeSubject(key, name);
    }

    @Override
    public void setComposeSubjects(List<Subject> subjects) {
        if(composeSubjectsAdapter != null)
            composeSubjectsAdapter.setSubjects(subjects);
    }

    private void chooseBirthday() {
        BirthdayPickerFragment fragment = new BirthdayPickerFragment();
        fragment.show(getActivity().getSupportFragmentManager(), getString(R.string.birthday));
    }

    @Override
    public void hideProgress() {
        binding.progress.progress.setVisibility(View.GONE);
    }

    @Override
    public void setTeacher(String lastName, String firstName, String middleName, String birthdayStr, String auditory, String phone) {
        binding.teacherLastName.clearFocus();
        binding.teacherLastName.setText(lastName);
        binding.teacherFirstName.setText(firstName);
        binding.teacherMiddleName.setText(middleName);
        binding.teacherAuditory.setText(auditory);
        binding.teacherPhone.setText(phone);
        binding.teacherBirthdayText.setText(birthdayStr);
        if(birthdayStr != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(Const.DD_MMM_YYYY);
            try {
                birthday = dateFormat.parse(birthdayStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        binding.teacherAdd.setText(R.string.change);
    }

    @Override
    public void finishWithResult() {
        if(getActivity() != null) {
            getActivity().setResult(Activity.RESULT_OK);
            getActivity().finish();
        }
    }

    public static class BirthdayPickerFragment extends AppCompatDialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Calendar calendar = Calendar.getInstance();
            if(birthday == null) {
                birthday = calendar.getTime();
            }
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this,
                    calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            dialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
            return dialog;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            birthday = calendar.getTime();

            if(getActivity() != null) {
                TextView birthdayView = getActivity().findViewById(R.id.teacher_birthday_text);
                SimpleDateFormat dateFormat = new SimpleDateFormat(Const.DD_MMM_YYYY);
                birthdayView.setText(dateFormat.format(birthday));
            }
        }
    }

}
