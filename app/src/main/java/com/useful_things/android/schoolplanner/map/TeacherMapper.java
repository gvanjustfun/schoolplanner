package com.useful_things.android.schoolplanner.map;

import com.useful_things.android.schoolplanner.Utils;
import com.useful_things.android.schoolplanner.data.entity.SubjectEntity;
import com.useful_things.android.schoolplanner.data.entity.TeacherEntity;
import com.useful_things.android.schoolplanner.data.entity.TeacherSubjectsEntity;
import com.useful_things.android.schoolplanner.ui.base.App;
import com.useful_things.android.schoolplanner.ui.subjects.entity.Subject;
import com.useful_things.android.schoolplanner.ui.teachers.entity.Teacher;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Function3;

public class TeacherMapper implements Function3<List<TeacherEntity>, List<TeacherSubjectsEntity>, List<SubjectEntity>, List<Teacher>> {
    @Override
    public List<Teacher> apply(List<TeacherEntity> teacherEntities, List<TeacherSubjectsEntity> teacherSubjectsEntities, List<SubjectEntity> subjectEntities) throws Exception {
        List<Teacher> teachers = new ArrayList<>();
        for(TeacherEntity teacherEntity : teacherEntities) {
            Teacher teacher = new Teacher();
            teacher.setHash(teacherEntity.getHash());
            teacher.setLastName(teacherEntity.getLastName());
            teacher.setFirstName(teacherEntity.getFirstName());
            teacher.setMiddleName(teacherEntity.getMiddleName());
            if(teacherEntity.getAuditory() != null) {
                teacher.setAuditory(teacherEntity.getAuditory());
            } else {
                teacher.setAuditory("");
            }
            teacher.setPhone(teacherEntity.getPhone());
            teacher.setBirthday(teacherEntity.getBirthday());

            List<Subject> subjects = new ArrayList<>();
            for(TeacherSubjectsEntity teacherSubjectsEntity : teacherSubjectsEntities) {
                if(teacherSubjectsEntity.getTeacher_hash().equals(teacherEntity.getHash())) {
                    boolean containsName = false;
                    for(SubjectEntity subjectEntity : subjectEntities) {
                        if(subjectEntity.getSubject_key().equals(teacherSubjectsEntity.getSubject_key())) {
                            if(subjectEntity.getSubject_name() != null) {
                                Subject subject = new Subject();
                                subject.setSubject_name(subjectEntity.getSubject_name());
                                subject.setSubject_key(subjectEntity.getSubject_key());
                                subjects.add(subject);
                                containsName = true;
                                break;
                            }
                        }
                    }

                    if(!containsName) {
                        Subject subject = new Subject();
                        int subjectStrId = Utils.getSubjectIdByKey(teacherSubjectsEntity.getSubject_key());
                        subject.setSubject_name(App.getApp().getString(subjectStrId));
                        subject.setSubject_key(teacherSubjectsEntity.getSubject_key());
                        subjects.add(subject);
                    }
                }
            }
            teacher.setSubjects(subjects);

            teachers.add(teacher);
        }
        return teachers;
    }
}
