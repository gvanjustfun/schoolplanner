package com.useful_things.android.schoolplanner.map;

import com.useful_things.android.schoolplanner.Const;
import com.useful_things.android.schoolplanner.Utils;
import com.useful_things.android.schoolplanner.data.entity.HomeworkEntity;
import com.useful_things.android.schoolplanner.data.entity.ScheduleEntity;
import com.useful_things.android.schoolplanner.data.entity.SubjectEntity;
import com.useful_things.android.schoolplanner.ui.base.App;
import com.useful_things.android.schoolplanner.ui.scheduler.SchedulePresenter;
import com.useful_things.android.schoolplanner.ui.scheduler.entity.Schedule;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.reactivex.functions.Function4;

public class ScheduleHomeworkMapper implements Function4<List<ScheduleEntity>, List<HomeworkEntity>, List<SubjectEntity>, String, List<Schedule[]>> {
    @Override
    public List<Schedule[]> apply(List<ScheduleEntity> scheduleEntities, List<HomeworkEntity> homeworkEntities, List<SubjectEntity> subjectEntities, String firstDate) throws Exception {
        List<Schedule[]> weekSchedule = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat(Const.DD_MMM_YYYY);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateFormat.parse(firstDate));
        int firstDay = calendar.get(Calendar.DAY_OF_YEAR);

        for(int dayOfWeek = 0;dayOfWeek < SchedulePresenter.WEEK_SIZE;dayOfWeek++) {
            calendar.set(Calendar.DAY_OF_YEAR, firstDay + dayOfWeek);

            String dateStr = dateFormat.format(calendar.getTime());
            List<Schedule> schedulesList = new ArrayList<>();
            for(ScheduleEntity scheduleEntity : scheduleEntities) {
                if(scheduleEntity.getDay_of_week() == dayOfWeek) {
                    Schedule schedule = new Schedule();
                    boolean containsName = false;
                    for (SubjectEntity subjectEntity : subjectEntities) {
                        if (subjectEntity.getSubject_key().equals(scheduleEntity.getSubject_key())) {
                            if (subjectEntity.getSubject_name() != null) {
                                schedule.setSubjectName(subjectEntity.getSubject_name());
                                containsName = true;
                                break;
                            }
                        }
                    }
                    if (!containsName) {
                        int subjectId = Utils.getSubjectIdByKey(scheduleEntity.getSubject_key());
                        schedule.setSubjectName(App.getApp().getString(subjectId));
                    }

                    int numberInDay = scheduleEntity.getNumber_in_day();
                    schedule.setNumberInDay(numberInDay);
                    for (HomeworkEntity homeworkEntity : homeworkEntities) {
                        if (homeworkEntity.getDate().equals(dateStr) &&
                                homeworkEntity.getNumber_in_day() == numberInDay) {
                            schedule.setHomework(homeworkEntity.getText());
                            schedule.setHomeworkDone(homeworkEntity.isDone());
                            break;
                        }
                    }
                    schedulesList.add(schedule);
                }
            }

            Schedule[] schedules = new Schedule[SchedulePresenter.DEFAULT_SCHEDULE_AMOUNT];
            for(Schedule schedule : schedulesList) {
                schedules[schedule.getNumberInDay()] = schedule;
            }

            for(int numberInDay = 0;numberInDay < schedules.length;numberInDay++) {
                if(schedules[numberInDay] == null) {
                    schedules[numberInDay] = new Schedule();
                }
                schedules[numberInDay].setDayOfWeek(dayOfWeek);
                schedules[numberInDay].setNumberInDay(numberInDay);
                schedules[numberInDay].setDate(dateStr);
            }

            weekSchedule.add(schedules);
        }


        return weekSchedule;
    }
}
