package com.useful_things.android.schoolplanner.ui.homework;

import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.useful_things.android.schoolplanner.Const;
import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.databinding.HomeworkBinding;

public class HomeworkViewImpl extends HomeworkView {

    private HomeworkBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        String dateStr = getArguments().getString(Const.DATE);
        int numberInDay = getArguments().getInt(Const.NUMBER_IN_DAY, 0);
        String homeworkText = getArguments().getString(Const.HOMEWORK_TEXT);

        presenter = new HomeworkPresenterImpl(dateStr, numberInDay);

        binding = DataBindingUtil.inflate(inflater, R.layout.homework, container, false);
        binding.homeworkAddBtn.setOnClickListener(v -> presenter.saveHomework());
        if(homeworkText == null || homeworkText.isEmpty()) {
            binding.homeworkAddBtn.setText(R.string.add);
        } else {
            binding.homeworkAddBtn.setText(R.string.change);
            binding.homeworkText.setText(homeworkText);
            binding.homeworkText.setSelection(homeworkText.length());
        }

        binding.homeworkText.requestFocus();
        InputMethodManager manager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        manager.showSoftInput(binding.homeworkText, InputMethodManager.SHOW_IMPLICIT);

        return binding.getRoot();
    }

    @Override
    public String getHomework() {
        return binding.homeworkText.getText().toString();
    }

    @Override
    public void finishActivity() {
        InputMethodManager manager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        manager.hideSoftInputFromWindow(binding.homeworkText.getWindowToken(), 0);

        getActivity().setResult(Activity.RESULT_OK);
        getActivity().finish();
    }

    @Override
    public boolean isDone() {
        return binding.homeworkDone.isChecked();
    }
}
