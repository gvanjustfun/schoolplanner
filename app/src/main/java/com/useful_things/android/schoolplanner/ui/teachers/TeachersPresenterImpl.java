package com.useful_things.android.schoolplanner.ui.teachers;

import com.useful_things.android.schoolplanner.data.entity.SubjectEntity;
import com.useful_things.android.schoolplanner.data.entity.TeacherEntity;
import com.useful_things.android.schoolplanner.data.entity.TeacherSubjectsEntity;
import com.useful_things.android.schoolplanner.map.TeacherMapper;
import com.useful_things.android.schoolplanner.ui.base.App;
import com.useful_things.android.schoolplanner.ui.teachers.entity.Teacher;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class TeachersPresenterImpl extends TeachersPresenter {
    @Override
    public void loadTeachers() {

        Observable<List<TeacherEntity>> teachersObservable = App.getApp().getDb().teacherDao().getAll()
                .toObservable()
                .subscribeOn(Schedulers.io());
        Observable<List<TeacherSubjectsEntity>> teacherSubjectsObservable = App.getApp().getDb().teacherSubjectsDao().getAll()
                .toObservable()
                .subscribeOn(Schedulers.io());
        Observable<List<SubjectEntity>> subjectObservable = App.getApp().getDb().subjectDao().getAll()
                .toObservable()
                .subscribeOn(Schedulers.io());

        Disposable disposable = Observable.zip(teachersObservable, teacherSubjectsObservable, subjectObservable, new TeacherMapper())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<Teacher>>() {
                    @Override
                    public void accept(List<Teacher> teachersResponse) throws Exception {
                        if(view == null) return;
                        view.hideProgress();

                        view.setTeachers(teachersResponse);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        if(view == null) return;
                        view.hideProgress();
                    }
                });
        compositeDisposable.add(disposable);
    }

    @Override
    public void openTeacher(String hash) {
        if(view == null) return;
        view.openTeacher(hash);
    }
}
