package com.useful_things.android.schoolplanner.ui.scheduler.view_holder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import com.useful_things.android.schoolplanner.R;
import com.useful_things.android.schoolplanner.Utils;
import com.useful_things.android.schoolplanner.databinding.ScheduleEntryBinding;
import com.useful_things.android.schoolplanner.ui.scheduler.SchedulePresenter;
import com.useful_things.android.schoolplanner.ui.scheduler.entity.Schedule;

public class ScheduleViewHolder extends RecyclerView.ViewHolder{

    private ScheduleEntryBinding binding;
    private SchedulePresenter presenter;

    public ScheduleViewHolder(@NonNull ScheduleEntryBinding binding, SchedulePresenter presenter) {
        super(binding.getRoot());
        this.binding = binding;
        this.presenter = presenter;
    }

    public void setSchedule(Schedule schedule) {
        binding.scheduleNumber.setText(String.valueOf(schedule.getNumberInDay() + 1));
        binding.scheduleSubject.setText(schedule.getSubjectName());
        binding.scheduleHomework.setText(schedule.getHomework());

        if (schedule.getNumberInDay() == SchedulePresenter.DEFAULT_SCHEDULE_AMOUNT - 1) {
            binding.scheduleNumberBlock.setBackgroundResource(R.drawable.cell_first_b);
            binding.scheduleSubjectBlock.setBackgroundResource(R.drawable.cell_middle_b);
            binding.scheduleHomeworkBlock.setBackgroundResource(
                    schedule.isHomeworkDone() ? R.drawable.cell_done_last_b : R.drawable.cell_last_b);
        } else {
            binding.scheduleNumberBlock.setBackgroundResource(R.drawable.cell_first);
            binding.scheduleSubjectBlock.setBackgroundResource(R.drawable.cell_middle);
            binding.scheduleHomeworkBlock.setBackgroundResource(
                    schedule.isHomeworkDone() ? R.drawable.cell_done_last : R.drawable.cell_last);
        }

        binding.scheduleSubjectBlock.setOnClickListener(v ->
                presenter.openSubjectsList(schedule.getDayOfWeek(), schedule.getNumberInDay()));
        binding.scheduleHomeworkBlock.setOnClickListener(v -> presenter.openHomework(schedule));
    }

}
