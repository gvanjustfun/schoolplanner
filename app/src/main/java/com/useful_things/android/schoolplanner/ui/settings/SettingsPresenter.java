package com.useful_things.android.schoolplanner.ui.settings;

import com.useful_things.android.schoolplanner.ui.base.BaseActivityPresenter;

public abstract class SettingsPresenter extends BaseActivityPresenter<SettingsView> {

    protected abstract void onChooseTheme();
    protected abstract void setTheme(int theme);
    protected abstract int getTheme();

}
