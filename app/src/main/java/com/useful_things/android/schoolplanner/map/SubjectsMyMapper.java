package com.useful_things.android.schoolplanner.map;

import com.useful_things.android.schoolplanner.Utils;
import com.useful_things.android.schoolplanner.data.entity.ScheduleEntity;
import com.useful_things.android.schoolplanner.data.entity.SubjectEntity;
import com.useful_things.android.schoolplanner.data.entity.TeacherEntity;
import com.useful_things.android.schoolplanner.data.entity.TeacherSubjectsEntity;
import com.useful_things.android.schoolplanner.ui.base.App;
import com.useful_things.android.schoolplanner.ui.subjects.entity.Subject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.functions.Function3;
import io.reactivex.functions.Function4;

public class SubjectsMyMapper implements Function4<List<SubjectEntity>, List<ScheduleEntity>, List<TeacherEntity>, List<TeacherSubjectsEntity>, List<Subject>> {
    @Override
    public List<Subject> apply(List<SubjectEntity> subjectEntities,
                               List<ScheduleEntity> scheduleEntities,
                               List<TeacherEntity> teacherEntities,
                               List<TeacherSubjectsEntity> teacherSubjectsEntities) throws Exception {
        List<Subject> subjects = new ArrayList<>();
        for(SubjectEntity subjectEntity : subjectEntities) {
            boolean existInSchedule = false;
            Subject subject = new Subject();
            for(ScheduleEntity scheduleEntity : scheduleEntities) {
                if(scheduleEntity.getSubject_key().equals(subjectEntity.getSubject_key())) {
                    existInSchedule = true;
                    subject.addDayOfWeek(scheduleEntity.getDay_of_week());
                }
            }

            if(existInSchedule) {
                if(subjectEntity.getSubject_name() == null) {
                    int nameId = Utils.getSubjectIdByKey(subjectEntity.getSubject_key());
                    subject.setSubject_name(App.getApp().getString(nameId));
                } else {
                    subject.setSubject_name(subjectEntity.getSubject_name());
                }
                subject.setSubject_key(subjectEntity.getSubject_key());

                List<String> teachers = new ArrayList<>();
                for(TeacherSubjectsEntity teacherSubjectsEntity : teacherSubjectsEntities) {
                    if(teacherSubjectsEntity.getSubject_key().equals(subjectEntity.getSubject_key())) {
                        String teacherHash =teacherSubjectsEntity.getTeacher_hash();
                        for(TeacherEntity teacherEntity : teacherEntities) {
                            if(teacherEntity.getHash().equals(teacherHash)) {
                                teachers.add(String.format("%s %s %s",
                                        teacherEntity.getFirstName(),
                                        teacherEntity.getMiddleName(),
                                        teacherEntity.getLastName()));
                                break;
                            }
                        }
                    }
                }
                subject.setTeachers(teachers);

                subjects.add(subject);
            }
        }

        Collections.sort(subjects, (o1, o2) -> o1.getSubject_name().compareTo(o2.getSubject_name()));

        return subjects;
    }
}
